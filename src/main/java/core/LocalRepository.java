package core;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents Local repository (.m2 folder)
 * It holds list of {@link VersionedLocalPackage} as sorted packages, localy stored.
 */
public class LocalRepository {

    /**
     * List of {@link VersionedLocalPackage} in repository.
     */
    private List<VersionedLocalPackage> versionedPackages;

    /**
     * Private constructor of the class
     * @param packages list of {@link LocalPackage} to load
     */
    private LocalRepository(List<LocalPackage> packages){
        sortVersions(packages);
    }

    /**
     * Static factory method
     * Creates instance of {@link LocalRepository} and fill it with {@link LocalPackage} list found in Path
     * If the Path is invalid or empty, returns null!
     * @param path path to Local Repository folder (.m2 folder)
     * @return instance of {@link LocalRepository} or null
     */
    public static LocalRepository createInstance(Path path){
        List<LocalPackage> lp = FileOperator.loadPackages(path);

        if(lp != null){ // If list of Local Packages is empty
            return new LocalRepository(lp);
        }
        return null;
    }

    /**
     * Private method!
     * Method sorts List of {@link LocalPackage} into List of {@link VersionedLocalPackage}
     * Basically it tries to fit all versions of some Artifact into one collection of the same name.
     * Then it sort the list of {@link VersionedLocalPackage} alphabeticaly.
     * @param packages list of {@link LocalPackage} to sort
     */
    private void sortVersions(List<LocalPackage> packages){
        versionedPackages = new ArrayList<>();

        //Picks 1 local package and tries to put it into existing VersionedLocalPackage with same ArtifactId
        for(LocalPackage l : packages){
            boolean versionedExists = false;
            for(VersionedLocalPackage v : versionedPackages){
                if(l.getArtifactId() != null){ //Just to be sure (Should not happen)
                    if(l.getArtifactId().equals(v.getName()) && l.getGroupId().equals(v.getGroupId())){
                        v.addVersion(l);
                        versionedExists = true;
                    }
                }
                else {
                    //Error output!
                    System.err.println("Error: ArtifactId is Null! " + l.getModel().getArtifactId() + " Path: " + l.getPath());
                    break;
                }
            }

            //If theres no VersionedLocalPackage for this ArtifactId, assume its first of its kind, lets create a new VersionedLocalPackage with the same name
            if(!versionedExists){
                if(l.getArtifactId() != null){
                    VersionedLocalPackage v = new VersionedLocalPackage(l.getArtifactId(), l.getGroupId(), l.getPath().getParent());
                    v.addVersion(l);
                    versionedPackages.add(v);
                }
            }
        }
        Collections.sort(versionedPackages);
    }

    /**
     * Method for searching in Local Artifacts (List of {@link VersionedLocalPackage})
     * If the searchSubstring contains String text its added to returning list.
     * @param text text to search for
     * @param sl Limit of searching
     * @return Alphabeticaly sorted list of {@link VersionedLocalPackage}
     */
    public List<VersionedLocalPackage> searchByName(String text, SearchLimiter sl){
        List<VersionedLocalPackage> list = new ArrayList<>();
        if(versionedPackages != null){
            for(VersionedLocalPackage vl : versionedPackages){
                String searchSubstring = null;
                switch (sl){
                    case ALL: searchSubstring = String.format("%s.%s", vl.getGroupId(), vl.getName()).toLowerCase();
                    break;
                    case GROUP_ID: searchSubstring = vl.getGroupId().toLowerCase();
                    break;
                    case ARTIFACT_ID: searchSubstring = vl.getName().toLowerCase();
                }

                if(searchSubstring.contains(text.toLowerCase())){
                    list.add(vl);
                }
            }
        }
        return list;
    }

    /**
     * Method for printing all {@link VersionedLocalPackage} in {@link LocalRepository}
     */
    public void printVersioned(){
        for(VersionedLocalPackage v : versionedPackages){
            System.out.println(v.toString());
        }
    }

    /**
     * Getter for list of all {@link VersionedLocalPackage} in repository
     * @return list of all {@link VersionedLocalPackage} in repository
     */
    public List<VersionedLocalPackage> getVersionedPackages(){
        return versionedPackages;
    }
}
