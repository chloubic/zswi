package core;

import java.nio.file.Path;
import java.util.*;
import java.util.List;

/**
 * Represents collection of all versions of one Artifact
 * Holds List of {@link LocalPackage}, list of all Versions of same Artifact
 */
public class VersionedLocalPackage implements Comparable<VersionedLocalPackage>{

    /**
     * List of all versions of Artifact
     */
    private List<LocalPackage> versionList = new ArrayList<>();

    /**
     * Name of Artifact (De facto ArtifactId)
     */
    private String name;

    /**
     * GroupId of Artifact
     */
    private String groupId;

    /**
     * Path to folder of Artifact versions
     */
    private Path fullPath;

    /**
     * Constructor of the class
     * Creates instance with name (ArtifactID), GroupId and path
     * @param name name of the Artifact
     * @param groupId GroupId of the Artifact
     * @param fullPath Path to the folder of Artifact Versions
     */
    public VersionedLocalPackage(String name, String groupId, Path fullPath){
        this.name = name;
        this.groupId = groupId;
        this.fullPath = fullPath;
    }

    /**
     * This method calls opening method in {@link FileOperator}
     */
    public void showInExplorer(){
        FileOperator.openInExplorer(fullPath);
    }

    /**
     * Reloads folders for all versions to re-check
     * Then sorts versions
     * @return sorted List of UPDATED versions
     */
    public List<LocalPackage> refresh(){
        versionList.clear();
        List<LocalPackage> list = FileOperator.loadPackages(fullPath);
        if(list != null){
            versionList.addAll(list);
        }
        sortVersions();
        return versionList;
    }

    /**
     * Just sorts Version list
     */
    public void sortVersions(){
        Collections.sort(versionList);
    }

    /**
     * Just Add version
     * @param lp Local package
     */
    public void addVersion(LocalPackage lp){
        versionList.add(lp);
    }

    /**
     * Just remove specified version
     * @param lp Specified version
     * @return operation response
     */
    public boolean removeVersion(LocalPackage lp){
        return versionList.remove(lp);
    }

    /**
     * Getter for name (ArtifactId)
     * @return name (ArtifactId)
     */
    public String getName(){
        return name;
    }

    /**
     * Getter for GroupId
     * @return GroupId
     */
    public String getGroupId(){
        return groupId;
    }

    /**
     * Getter for versions
     * @return List of versions
     */
    public List<LocalPackage> getVersions(){
        return versionList;
    }

    /**
     * Overrided method toString()
     * @return Text representation of the instance
     */
    @Override
    public String toString(){
        Collections.sort(versionList);
        String ret = "Artifact: " + name + " (Group: " + groupId + ") versions: \n";
        for(LocalPackage l : versionList){
            ret += "    " + l.toString() + "\n";
        }
        return ret;
    }

    /**
     * Overrided comparator method, by name (ArtifactId)
     * @param o Another {@link VersionedLocalPackage}
     * @return Comparsion result
     */
    @Override
    public int compareTo(VersionedLocalPackage o) {
        return this.getName().toLowerCase().compareTo(o.getName().toLowerCase());
    }
}
