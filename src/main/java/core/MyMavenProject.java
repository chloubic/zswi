package core;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;

import java.util.ArrayList;
import java.util.List;


/**
 * Represents Maven Project.
 * It holds an instance of model from project .pom file.
 */
public class MyMavenProject {

    /**
     * Instance of projects .pom file
     */
    private Model model;

    /**
     * Constructor of the class
     * @param m model of the project
     */
    public MyMavenProject(Model m){
        this.model = m;
    }

    /**
     * This method return list of dependencies of the project in instances of {@link LocalPackage}.
     * If dependency exists in {@link LocalRepository}, it return its loaded instance.
     * If dependency is missing, it will return fake LocalPackage (Without any data, marked as empty)
     * @param lr instance of Local Repository
     * @return list of {@link LocalPackage}
     */
    public List<LocalPackage> getDependencies(LocalRepository lr){
        List<LocalPackage> list = new ArrayList<>();

        for(Object d : model.getDependencies()){
            Dependency dep = (Dependency)d;
            LocalPackage l = LocalPackage.createFromDependency(dep, lr);
            list.add(l);
        }

        return list;
    }

    /**
     * Getter for {@link Model} of project
     * @return {@link Model} instance of the project
     */
    public Model getModel(){
        return model;
    }
}
