package core;


/**
 * This enum represents search possibilities for {@link VersionedLocalPackage} in {@link LocalRepository}
 */
public enum SearchLimiter {
    ALL("All"), ARTIFACT_ID("ArtifactId"), GROUP_ID("GroupId");

    private String action;

    SearchLimiter(String action){
        this.action = action;
    }

    @Override
    public String toString(){ return this.action; }
}
