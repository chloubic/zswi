package core;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

/**
 * Represents one version of Artifact in Local repository .m2 folder
 */
public class LocalPackage implements Comparable<LocalPackage>{

    /**
     * Path to folder
     */
    private Path fullPath;

    /**
     * Instance of model (.pom file)
     */
    private Model model;

    /**
     * Boolean if folder contains class files
     */
    private Boolean isClass = false;

    /**
     * Boolean if folder contains source files
     */
    private Boolean isSource = false;

    /**
     * Boolean if folder contains Javadoc files
     */
    private Boolean isJavadoc = false;

    /**
     * Boolean if folder exist (Or its fake package)
     */
    private Boolean exists = true;

    /**
     * Name of the Artifact (If unavailable ArtifactId is used)
     */
    private String name;

    /**
     * ArtifactId of this artifact
     */
    private String artifactId;

    /**
     * GroupId of this Artifact
     */
    private String groupId;

    /**
     * Version of this Artifact
     */
    private String version;

    /**
     * Constructor of the class
     * Creates instance from model and path
     * @param model model of the .pom file of the Artifact
     * @param path path to the Artifact
     */
    public LocalPackage(Model model, Path path){
        this.model = model;
        this.fullPath = path;
        this.version = model.getVersion();
        this.name = model.getName();
        this.groupId = model.getGroupId();
        this.artifactId = model.getArtifactId();

        //Not null corrections
        if(version == null){
            if(model.getParent() != null)
                version = model.getParent().getVersion(); //If Artifact dont have version itself ... Parent should have
        }
        if(name == null){
            name = model.getArtifactId(); //No name set, using ArtifactId instead
        }
        if(groupId == null){
            if(model.getParent() != null){
                groupId = model.getParent().getGroupId();//If Artifact dont have GroupId itself ... Parent should have
            }
        }

        initialize(fullPath);
    }

    /**
     * Private constructor for creating fake {@link LocalPackage} from {@link Dependency}
     * @param dep {@link Dependency} as data source
     */
    private LocalPackage(Dependency dep){
        this.name = dep.getArtifactId();
        this.artifactId = dep.getArtifactId();
        this.groupId = dep.getGroupId();
        this.version = dep.getVersion();
        this.exists = false;
    }

    /**
     * Static factory method to create {@link LocalPackage} from {@link Dependency}
     * If found in repository, return existing instance, if not, Creates Fake {@link LocalPackage}
     * @param dep {@link Dependency} as data source
     * @param lr instance of {@link LocalRepository}
     * @return instance of {@link LocalPackage}
     */
    public static LocalPackage createFromDependency(Dependency dep, LocalRepository lr){
        List<VersionedLocalPackage> list = lr.searchByName(dep.getGroupId() + "." + dep.getArtifactId(), SearchLimiter.ALL);
        if(list.size() > 0){
            for(LocalPackage l : list.get(0).getVersions()){
                if(l.getVersion().equals(dep.getVersion())){
                    return l;
                }
            }
        }
        return new LocalPackage(dep);
    }

    /**
     *Investigates the folder and sets booleans for:
     * {@code isJavadoc}
     * {@code isSources}
     * {@code isClass}
     * @param path path to folder to investigate
     */
    private void initialize(Path path){
        File[] files;
        files = new File(path.toString()).listFiles();
        if(files != null || files.length == 0){
            for(File f : files){
                String ext = f.getName().substring(f.getName().lastIndexOf(".") + 1);
                if(ext.equals("jar")){
                    if(f.getName().toLowerCase().endsWith("javadoc.jar")){
                        isJavadoc = true;
                    } else if(f.getName().toLowerCase().endsWith("sources.jar")){
                        isSource = true;
                    } else {
                        isClass = true;
                    }
                }
            }
        }
    }

    /**
     * This method calls opening method in {@link FileOperator} if this is not Fake Package
     */
    public void showInExplorer(){
        if(exists) {
            FileOperator.openInExplorer(fullPath);
        }
    }

    /**
     * Getter for Path
     * @return Path
     */
    public Path getPath(){
        return fullPath;
    }

    /**
     * Getter for model
     * @return model
     */
    public Model getModel(){
        return model;
    }

    /**
     * Getter for version
     * @return version
     */
    public String getVersion(){
        return version;
    }

    /**
     * Getter for name
     * @return name
     */
    public String getName(){
        return name;
    }

    /**
     * Getter for GroupId
     * @return GroupId
     */
    public String getGroupId(){
        return groupId;
    }

    /**
     * Getter for ArtifactId
     * @return ArtifactID
     */
    public String getArtifactId() {
        return artifactId;
    }

    /**
     * Value of isClass
     * @return Returns true, if folder contains class files, else otherwise
     */
    public Boolean isClass() {
        return isClass;
    }

    /**
     * Value of isSource
     * @return Returns true, if folder contains source files, else otherwise
     */
    public Boolean isSource() {
        return isSource;
    }

    /**
     * Value of isJavadoc
     * @return Returns true, if folder contains Javadoc files, else otherwise
     */
    public Boolean isJavadoc() {
        return isJavadoc;
    }

    /**
     * Value of exists
     * @return Returns true if folder exists, else if this instance is fake package
     */
    public Boolean exists(){
        return exists;
    }

    /**
     * Overrided method of toString()
     * @return String representation of instance
     */
    @Override
    public String toString(){
        return String.format("Name: %s (Version: %s) {Path: %s} {Class %b} {Sources %b} {Javadoc %b}", getName(), version, fullPath, isClass, isSource, isJavadoc);
    }

    /**
     * Overrided comparator method, to compare versions
     * @param o Another {@link LocalPackage}
     * @return Value of comparsion
     */
    @Override
    public int compareTo(LocalPackage o) {
        if(this.getVersion() == null || o.getVersion() == null){
            return 0;
        }
        return this.getVersion().compareTo(o.getVersion());
    }
}
