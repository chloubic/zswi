package core;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Static class for file Operations
 */
public class FileOperator {

    /**
     * This method loads all Packages from given Path
     * @param path Path to folder
     * @return List of {@link LocalPackage} if available, if not, null is returned (even if folder is empty!)
     */
    public static List<LocalPackage> loadPackages(Path path){
        List<Path> pomPaths = loadPoms(path);
        if(pomPaths != null){
            List<LocalPackage> list = loadModels(pomPaths);
            return list;
        }

        return null;
    }

    /**
     * Loader for Projects .pom
     * @param path Path to .pom file
     * @return instance of {@link MyMavenProject} or null, if .pom is invalid
     */
    public static MyMavenProject loadProject(Path path){
        Model m = loadModel(path);
        if(m != null){
            return new MyMavenProject(m);
        }
        return null;
    }

    /**
     * Loads all paths to .pom files from given Path
     * @param folder Path to folder
     * @return List of Paths to .pom files or null if invalid
     */
    private static List<Path> loadPoms(Path folder){
        File fileRepository = new File(folder.toString());
        if(fileRepository.exists()){
            try (var path = Files.walk(folder)) {
                 return path.filter(Files::isRegularFile)
                        .filter(f -> f.toString().endsWith(".pom"))
                        .collect(Collectors.toList());
            } catch (IOException e) {
                System.err.println("ERROR: IO Exception!");
                e.printStackTrace();
            }

        } else {
            return null;
        }

        return null;
    }

    /**
     * Loads all {@link LocalPackage} from given lst of Paths
     * @param pomPaths list of Paths to .pom files
     * @return list of {@link LocalPackage} or null if empty
     */
    private static List<LocalPackage> loadModels(List<Path> pomPaths) {
        List<LocalPackage> list = new ArrayList<>();
        for(Path p : pomPaths){
            Model m = loadModel(p);
            if(m != null){
                list.add(new LocalPackage(m, p.getParent()));
            }
        }
        if(list.size() == 0){
            return null;
        }
        return list;
    }

    /**
     * Loads single .pom file into {@link Model}
     * @param path Path to .pom file
     * @return Instance if {@link Model} or null, if path is invalid
     */
    private static Model loadModel(Path path) {
        try (var br = new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile())))) {
            var reader = new MavenXpp3Reader();
            return reader.read(br);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Method to open given path in native system Explorer
     * @param path Path to open
     */
    public static void openInExplorer(Path path){
        if (Desktop.isDesktopSupported()) {
            new Thread(() -> {
                try {
                    Desktop.getDesktop().open(new File(path.toString()));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }).start();
        }
    }
}
