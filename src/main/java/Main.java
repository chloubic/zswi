import graphics.WaitWindow;
import graphics.Window;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class which starts the application.
 */
public class Main extends Application {

    /**
     * Calls launch method with console parameters.
     * @param args Console parameters array
     */
    public static void main(String[] args){
        launch(args);
    }

    /**
     * Method creates stage of the application (setups {@link Window}).
     * @param stage Instance of application {@link Stage}
     */
    public void start(Stage stage) {
        WaitWindow waitWindow = new WaitWindow();
        waitWindow.show();
        Window window = new Window(stage, waitWindow);
        Scene root = new Scene(window, 1000, 600);
        stage.widthProperty().addListener((obs, oldVal, newVal) -> window.windowWidthChange(newVal.doubleValue()));
        stage.heightProperty().addListener((obs, oldVal, newVal) -> window.windowHeightChange(newVal.doubleValue()));
        stage.setMinWidth(1000);
        stage.setMinHeight(600);
        stage.setScene(root);
        stage.setMaximized(true);
        stage.setTitle("Maven Local Repository");
    }
}
