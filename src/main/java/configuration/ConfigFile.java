package configuration;

import graphics.Window;
import javafx.scene.control.Alert;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents structure of configuration file. *
 */
public class ConfigFile extends File {

    /**
     * Pointer to instance of {@link Window}.
     */
    private Window window;

    /**
     * Name and file extension of configuration file.
     */
    private final static String CONFIG_FILE_NAME = "configuration.json";

    /**
     * Actual parameter of .m2 repository path (synchronized with config file).
     */
    private String m2Path;

    /**
     * Posts per page value.
     */
    private int postPerPage;

    /**
     * List of actual projects, opened by used (synchronized with config file).
     */
    private List<JSONObject> projectsList;

    /**
     * User root of runtime system.
     */
    private static final String USER_ROOT = System.getProperty("user.home");

    /**
     * Default file separator of runtime system.
     */
    private static final String SEPARATOR = File.separator;

    /**
     * Constructor setups and initializes configuration file and metadata.
     * @param window Pointer to instance of {@link Window}.
     */
    public ConfigFile(Window window) {
        super(CONFIG_FILE_NAME);
        this.window = window;
        this.projectsList = new ArrayList<>();
        initializeConfig();
    }

    /**
     * Initializes configuration file.
     * Creates new file and creates skeleton of config if not exists.
     * Otherwise initializes local variables only.
     */
    private void initializeConfig() {
        if(!this.exists()){
            try {
                this.createNewFile();
                this.writeSkeleton(this.createSkeleton());
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Configuration file not created! When you exit, all settings disappear.");
                alert.show();
            }
            initializeVariables();
        } else {
            initializeVariables();
        }
    }

    /**
     * Initializes actual local variables from configuration file on application startup.
     * .m2 repozitory path loads into {@link #m2Path} variable.
     * Projects load into {@link #projectsList} variable in {@link JSONObject} format.
     */
    private void initializeVariables() {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(this))
        {
            Object obj = jsonParser.parse(reader);

            JSONObject object = (JSONObject) obj;
            this.m2Path = (String) object.get("m2Path");
            this.postPerPage = Integer.parseInt((String) object.get("postPerPage"));

            JSONArray projects = (JSONArray) object.get("projects");
            projects.forEach(proj -> this.projectsList.add((JSONObject) proj));
            this.sortProjects();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes new project into actual list of projects and triggers synchronization.
     * @param projectName New project name
     * @param projectPath New project path
     */
    public void writeNewProject(String projectName, String projectPath){
        JSONObject projectDetails = new JSONObject();
        projectDetails.put("name", projectName);
        projectDetails.put("path", projectPath);
        projectDetails.put("lastUpdate", String.format("%d", System.currentTimeMillis()));

        this.projectsList.add(projectDetails);
        this.synchronize();
    }

    /**
     * Changes actual .m2 repozitory path and triggers synchronization.
     * @param m2Path New .m2 repozitory path
     */
    public void writeM2Location(String m2Path){
        this.m2Path = m2Path;
        this.synchronize();
    }

    /**
     * Changes actual post per page value and triggers synchronization.
     * @param postPerPage New post per page value
     */
    public void writePostPerPage(int postPerPage){
        this.postPerPage = postPerPage;
        this.synchronize();
    }

    /**
     * Removes project from list.
     * @param project Project to be removed
     */
    public void removeProject(JSONObject project){
        this.projectsList.remove(project);
        this.synchronize();
    }

    /**
     * After projects initialization sorts projects by time.
     */
    public void sortProjects(){
        for (int i = 0; i < this.projectsList.size()-1; i++)
            for (int j = 0; j < this.projectsList.size()-i-1; j++) {
                if (Long.parseLong(this.projectsList.get(j).get("lastUpdate").toString())
                        < Long.parseLong(this.projectsList.get(j + 1).get("lastUpdate").toString())) {
                    this.projectsList.add(j, this.projectsList.remove(j + 1));
                }
            }
    }

    /**
     * Synchronizes config file with actual parameters.
     */
    private void synchronize(){
        try (FileWriter file = new FileWriter(this)) {
            JSONObject object = new JSONObject();

            JSONArray projectsArray = new JSONArray();

            object.put("m2Path", this.m2Path);

            object.put("postPerPage", String.format("%d", this.postPerPage));

            this.projectsList.forEach(proj -> projectsArray.add(proj));

            object.put("projects", projectsArray);

            file.write(object.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes initialized skeleton of config into file.
     * @param skeleton Initialized JSON skeleton of configuration
     */
    private void writeSkeleton(JSONObject skeleton){
        try (FileWriter file = new FileWriter(this)) {

            file.write(skeleton.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates JSON skeleton of configuration.
     * @return {@link JSONObject} skeleton of configuration
     */
    private JSONObject createSkeleton(){
        JSONObject object = new JSONObject();

        JSONArray projectsArray = new JSONArray();

        object.put("m2Path", USER_ROOT + SEPARATOR + ".m2");
        object.put("postPerPage", "20");
        object.put("projects", projectsArray);

        return object;
    }

    /**
     * Returns actual .m2 repozitory path.
     * @return Actual .m2 repozitory path
     */
    public String getM2Path(){ return this.m2Path; }

    /**
     * Returns posts per page.
     * @return Posts per page
     */
    public int getPostPerPage(){ return this.postPerPage; }

    /**
     * Return projects list.
     * @return Projects list
     */
    public List<JSONObject> getProjectsList() { return projectsList; }

    /**
     * Search for projects by name.
     * @param search Search string
     * @return List of found projects
     */
    public List<JSONObject> getProjectsByName(String search){
        List<JSONObject> searchedProjects = new ArrayList<>();
        for(JSONObject proj : this.projectsList){
            if(proj.get("name").toString().toLowerCase().contains(search.toLowerCase()))
                searchedProjects.add(proj);
        }
        return searchedProjects;
    }

    /**
     * Synchronizes projects after change.
     */
    public void projectChanged() { this.synchronize(); }
}