package graphics.sections;

import graphics.Window;
import graphics.menu.MainMenu;
import graphics.sections.components.ISection;
import graphics.sections.components.settings.choiceboxes.LanguageChoiceBox;
import graphics.sections.components.settings.labels.*;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import org.codehaus.plexus.util.StringUtils;

import java.io.File;

/**
 * Represents app settings scene.
 */
public class Settings extends VBox implements ISection {

    /**
     * Instance of {@link Window}.
     */
    private Window window;

    /**
     * {@link DirectoryChooser} for choosing .m2 directory path.
     */
    private DirectoryChooser directoryChooser;

    /**
     * Settings label.
     */
    private Label settingsLabel;

    /**
     * Language label.
     */
    private Label languagelabel;

    /**
     * Language chooser.
     */
    private ChoiceBox<String> languageChoiceBox;

    /**
     * M2 location label.
     */
    private Label m2Location;

    /**
     * Instance of "per page" heading.
     */
    private Label postPerPage;

    /**
     * Field of "per page" attribute.
     */
    private TextField postPerPageInput;

    /**
     * Instance of {@link Button} for save "per page" attribute.
     */
    private Button applyPostPerPage;

    /**
     * M2 location display field.
     */
    private TextField m2Path;

    /**
     * M2 path change button.
     */
    private Button changeM2Path;

    /**
     * About label.
     */
    private Label aboutLabel;

    /**
     * Description label.
     */
    private Label descriptionLabel;

    /**
     * App version label.
     */
    private Label versionLabel;

    /**
     * Copyright label.
     */
    private Label copyrightsLabel;

    /**
     * Initializes and creates settings scene.
     * @param window Instance of {@link Window}
     */
    public Settings(Window window){
        super();
        this.window = window;
        this.initialize();
        this.setUp();
        this.initializeEvent();
    }

    /**
     * Initializes settings.
     */
    private void initialize(){
        this.directoryChooser = new DirectoryChooser();
        this.settingsLabel = new SettingsLabel();
        this.languagelabel = new LanguageLabel();
        this.languageChoiceBox = new LanguageChoiceBox();
        this.aboutLabel = new AboutLabel();
        this.m2Location = new Label(".m2 location");
        this.m2Path = new TextField();
        this.changeM2Path = new Button("Change .m2 location");
        this.postPerPage = new Label("Packages/versions per page");
        this.postPerPageInput = new TextField();
        this.applyPostPerPage = new Button("Apply");
        this.descriptionLabel = new DescriptionLabel();
        this.versionLabel = new VersionLabel();
        this.copyrightsLabel = new CopyrightsLabel();
    }

    /**
     * Setups settings.
     */
    private void setUp(){
        this.directoryChooser.setTitle("Select .m2 location");
        this.directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        this.m2Location.setTextFill(Color.web("#323232"));
        this.m2Location.setFont(new Font(20.0));
        this.m2Location.setPadding(new Insets(20, 0, 0, 0));

        HBox m2HBox = new HBox();
        m2HBox.setSpacing(20);
        m2HBox.setPadding(new Insets(10, 0, 0, 0));

        this.m2Path.setText(this.window.getConfigFile().getM2Path());
        this.m2Path.setEditable(false);
        this.m2Path.setStyle("-fx-background-color: transparent");

        this.changeM2Path.setTextFill(Color.web("#375e97"));
        this.changeM2Path.setStyle("-fx-background-color: transparent; -fx-border-color: #375e97; -fx-border-radius: 5px;  -fx-font-weight: 900; -fx-font-family: Verdana, Arial, sans-serif");

        m2HBox.getChildren().addAll(this.m2Path, this.changeM2Path);

        HBox perPageHBox = new HBox();
        perPageHBox.setSpacing(20);
        perPageHBox.setPadding(new Insets(10, 0, 0, 0));

        this.postPerPage.setTextFill(Color.web("#323232"));
        this.postPerPage.setFont(new Font(20.0));
        this.postPerPage.setPadding(new Insets(20, 0, 0, 0));

        this.postPerPageInput.setText(String.format("%s", this.window.getConfigFile().getPostPerPage()));
        this.postPerPageInput.setStyle("-fx-background-color: transparent; -fx-font-family: Verdana, Arial, sans-serif");
        this.postPerPageInput.setPrefWidth(60);

        this.applyPostPerPage.setTextFill(Color.web("#375e97"));
        this.applyPostPerPage.setStyle("-fx-background-color: transparent; -fx-border-color: #375e97; -fx-border-radius: 5px; -fx-font-weight: 900; -fx-font-family: Verdana, Arial, sans-serif");

        perPageHBox.getChildren().addAll(this.postPerPageInput, this.applyPostPerPage);

        this.getChildren().addAll(
                this.settingsLabel,
                this.languagelabel,
                this.languageChoiceBox,
                this.m2Location,
                m2HBox,
                this.postPerPage,
                perPageHBox,
                this.aboutLabel,
                this.descriptionLabel,
                this.versionLabel,
                this.copyrightsLabel
        );

        this.setPadding(new Insets(30, 30, 30, 30));
    }

    /**
     * Initializes button events.
     */
    private void initializeEvent(){
        this.changeM2Path.setOnMouseClicked(e -> {
            File directory = this.directoryChooser.showDialog(this.window.getStage());
            if(directory != null) {
                this.m2Path.setText(directory.getAbsolutePath());
                this.window.getConfigFile().writeM2Location(directory.getAbsolutePath());
                this.window.getMainMenu().flashSuccessMessage("Path successfully changed");
            }
            else {
                this.window.getMainMenu().flashInfoMessage("Path unchanged");
            }
        });
        this.applyPostPerPage.setOnMouseClicked(e -> this.handlePerPageChange());
        this.postPerPageInput.setOnAction(e -> this.handlePerPageChange());
    }

    /**
     * Method for handling change of "per page" attribute.
     */
    private void handlePerPageChange(){
        if(StringUtils.isNumeric(this.postPerPageInput.getText())) {
            int newValue = Integer.parseInt(this.postPerPageInput.getText());
            if (newValue > 0) {
                this.window.getConfigFile().writePostPerPage(newValue);
                this.window.getDashboard().getDashboardPagination().setPageInput(1);
                this.window.getDashboard().getDashboardPagination().setPageCount((int) Math.ceil(this.window.getDashboard().getAvailablePackages().size() / (double) this.window.getConfigFile().getPostPerPage()));
                this.window.getDashboard().getDashboardPagination().checkButtons();
                this.window.getProjects().getProjectsPagination().setPageCount((int)Math.ceil(this.window.getProjects().getProjectsAvailable().size() / (double)this.window.getConfigFile().getPostPerPage()));
                this.window.getProjects().getProjectsPagination().setPageInput(1);
                this.window.getProjects().getProjectsPagination().checkButtons();
                this.window.getDashboard().triggerPageRefresh();
                this.window.getProjects().triggerPageRefresh();
                this.window.getMainMenu().flashSuccessMessage("Packages/versions per page successfully changed");
            }
            else{
                this.window.getMainMenu().flashFailMessage("Value unchanged. Enter valid number greater than 0");
                this.postPerPageInput.setText(String.format("%s", this.window.getConfigFile().getPostPerPage()));
            }
        }
        else{
            this.window.getMainMenu().flashFailMessage("Value unchanged. Enter valid number greater than 0");
            this.postPerPageInput.setText(String.format("%s", this.window.getConfigFile().getPostPerPage()));
        }
    }

    /**
     * Clears additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void clear(MainMenu mainMenu) { }

    /**
     * Sets additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void set(MainMenu mainMenu) { }

    /**
     * Width change event handler and recalculator.
     * @param newWidth New window width
     */
    public void windowWidthChange(double newWidth) { }

    /**
     * Height change event handler.
     * @param newHeight New window height
     */
    public void windowHeightChange(double newHeight) { }

    /**
     * Method for page refresh triggering.
     */
    @Override
    public void triggerPageRefresh() { }
}
