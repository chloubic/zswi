package graphics.sections;

import core.MyMavenProject;
import graphics.Window;
import graphics.menu.MainMenu;
import graphics.sections.components.ISection;
import graphics.sections.components.projects.Filters;
import graphics.sections.components.projects.Project;
import graphics.sections.components.projects.ProjectsPagination;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents projects scene.
 */
public class Projects extends ScrollPane implements ISection {

    /**
     * Instance of main {@link Window}.
     */
    private Window window;

    /**
     * Instance of opened project details scene.
     */
    private ProjectDetails projectDetails;

    /**
     * Main layout pane.
     */
    private Pane pane;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * List of available projects.
     */
    private List<Project> projectsAvailable;

    /**
     * Instance of projects filters.
     */
    private Filters filters;

    /**
     * Instance of projects pagination.
     */
    private ProjectsPagination projectsPagination;

    /**
     * Projects per row variable.
     */
    private double projectsPerRow = 1;

    /**
     * Initializes and creates projects scene.
     * @param window Instance of {@link Window} scene
     */
    public Projects(Window window) {
        super();
        this.window = window;
        this.initialize();
        this.setUp();
        this.initializeProjects();
    }

    /**
     * Initializes projects scene.
     */
    private void initialize() {
        pane = new Pane();
        vBox = new VBox();
        this.projectsAvailable = new ArrayList<>();
        this.filters = new Filters(this);
        this.projectsPagination = new ProjectsPagination(this);
    }

    /**
     * Initializes projects.
     */
    private void initializeProjects(){
        for(JSONObject project : this.window.getConfigFile().getProjectsList())
            this.addProject(new Project(this, project));
        this.projectsPagination.setPageCount((int)Math.ceil(this.projectsAvailable.size() / (double)this.window.getConfigFile().getPostPerPage()));
    }

    /**
     * Initializes searched projects.
     * @param searchedProjects Searched projects
     */
    public void displaySearchedPackages(List<JSONObject> searchedProjects){
        this.projectsPagination.setPageInput(1);
        this.projectsAvailable = new ArrayList<>();
        for(JSONObject proj : searchedProjects)
            this.projectsAvailable.add(new Project(this, proj));
        this.reorganizeElements(this.vBox, this.projectsPerRow);
        this.projectsPagination.setPageCount((int)Math.ceil(this.projectsAvailable.size() / (double)this.window.getConfigFile().getPostPerPage()));
        this.projectsPagination.checkButtons();
    }

    /**
     * Setups projects scene.
     */
    private void setUp(){
        HBox hBox = new HBox();
        hBox.setSpacing(30);
        vBox.getChildren().add(hBox);
        vBox.setPadding(new Insets(30, 30, 30, 30));
        vBox.setSpacing(30);
        pane.getChildren().add(vBox);
        this.setContent(pane);
        this.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pane.setStyle("-fx-background-color: #fff");
    }

    /**
     * Adds new project.
     * @param project New project
     */
    public void addProject(Project project) {
        this.projectsAvailable.add(project);
        if(this.projectsAvailable.size() <= this.window.getConfigFile().getPostPerPage()) {
            Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
            if (hBox instanceof HBox) {
                if (((HBox) hBox).getChildren().size() >= projectsPerRow) {
                    HBox newHbox = new HBox();
                    newHbox.setSpacing(30);
                    newHbox.getChildren().add(project);
                    this.vBox.getChildren().add(newHbox);
                } else {
                    ((HBox) hBox).getChildren().add(project);
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot add new package to the list - HBox did not found");
                alert.show();
            }
        }
    }

    /**
     * Width change event handler and recalculator.
     * @param newWidth New window width
     */
    public void windowWidthChange(double newWidth) {
        this.pane.setPrefWidth(newWidth - 30);
        Node nodeOut = pane.getChildren().get(0);
        if(nodeOut instanceof VBox){
            double versionsPerRow = Math.floor((this.window.getStage().getWidth() - 90) / (Project.MAX_WIDTH)) - 1;
            if(versionsPerRow == this.projectsPerRow) {
                for(Node nodeIn:((VBox)nodeOut).getChildren()){
                    if(nodeIn instanceof HBox){
                        ((HBox) nodeIn).setPrefWidth(this.pane.getWidth() - 60);
                        for (Node hBoxNodes : ((HBox) nodeIn).getChildren()) {
                            if (hBoxNodes instanceof GridPane) {
                                ((Project) hBoxNodes).widthChangeEventHandler();
                            }
                        }
                    }
                }
            } else reorganizeElements((VBox) nodeOut, versionsPerRow);
        }
    }

    /**
     * Recalculates and reorganizes elements.
     * @param nodeOut First {@link VBox} node
     * @param count Count of projects per row
     */
    private void reorganizeElements(VBox nodeOut, double count) {
        this.projectsPerRow = count;
        nodeOut.getChildren().clear();

        HBox initialHBox = new HBox();
        initialHBox.setSpacing(30);

        this.vBox.getChildren().add(initialHBox);

        int startIndex = (this.projectsPagination.getActualPage() - 1) * this.window.getConfigFile().getPostPerPage();
        for(int i = startIndex; i < (startIndex + this.window.getConfigFile().getPostPerPage()); i++) {
            if (i < this.projectsAvailable.size()) {
                Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
                if (hBox instanceof HBox) {
                    if (((HBox) hBox).getChildren().size() >= projectsPerRow) {
                        HBox newHbox = new HBox();
                        newHbox.setSpacing(30);
                        newHbox.getChildren().add(this.projectsAvailable.get(i));
                        this.vBox.getChildren().add(newHbox);
                    } else {
                        ((HBox) hBox).getChildren().add(this.projectsAvailable.get(i));
                    }
                }
            }
        }

        this.windowWidthChange(this.window.getStage().getWidth());
    }

    /**
     * Height change event handler.
     * @param newHeight New window height
     */
    public void windowHeightChange(double newHeight) {
        this.pane.setMinHeight(newHeight - 20);
    }

    /**
     * Method that triggers page refresh.
     */
    @Override
    public void triggerPageRefresh() { this.reorganizeElements(this.vBox, this.projectsPerRow); }

    /**
     * Clears additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void clear(MainMenu mainMenu) {
        mainMenu.removeAdditionalPanel(this.filters);
        this.window.setBottom(null);
    }

    /**
     * Sets additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void set(MainMenu mainMenu) {
        mainMenu.addAdditionalPanel(this.filters);
        this.window.setBottom(this.projectsPagination);
        this.projectsPagination.checkButtons();
        if(this.projectDetails != null){
            this.window.clear();
            this.window.setActualSection(this.projectDetails);
            this.window.initializeContent();
        }
    }

    /**
     * Opens project details.
     * @param project Project to be displayed
     * @param myMavenProject Instance of maven project
     */
    public void openProjectDetails(Project project, MyMavenProject myMavenProject) {
        if(this.window.getLocalRepository() == null){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Instance of Local repository could not be created!");
            alert.show();
            return;
        }
        this.projectsAvailable.remove(project);
        this.projectsAvailable.add(0, project);
        project.getProject().put("lastUpdate", String.format("%d", System.currentTimeMillis()));
        this.window.getConfigFile().projectChanged();
        this.projectDetails = new ProjectDetails(project, this, myMavenProject);
        this.window.clear();
        this.window.setActualSection(this.projectDetails);
        this.window.initializeContent();
        this.window.fireResizeEvent();
        this.triggerPageRefresh();
    }

    /**
     * Closes project details.
     */
    public void closeProjectDetails() {
        this.projectDetails = null;
        this.window.clear();
        this.window.setActualSection(this);
        this.window.initializeContent();
        this.window.fireResizeEvent();
    }

    /**
     * Refreshes pagination of projects page on bottom.
     */
    public void refreshPagination(){
        this.getProjectsPagination().setPageCount((int)Math.ceil(this.getProjectsAvailable().size() / (double)this.getWindow().getConfigFile().getPostPerPage()));
        this.getProjectsPagination().setPageInput(this.getProjectsPagination().getActualPage());
        this.getProjectsPagination().checkButtons();
    }

    /**
     * Returns {@link Window} instance.
     * @return {@link Window} instance
     */
    public Window getWindow(){ return this.window; }

    /**
     * Returns list of available projects.
     * @return List of available projects
     */
    public List<Project> getProjectsAvailable() { return projectsAvailable; }

    /**
     * Returns project details instance.
     * @return Project details instance
     */
    public ProjectDetails getProjectDetails() { return projectDetails; }

    /**
     * Returns projects pagination.
     * @return Projects pagination
     */
    public ProjectsPagination getProjectsPagination(){ return this.projectsPagination; }
}
