package graphics.sections.components.filters.choiceboxes;

import graphics.sections.components.filters.SortBy;
import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;

/**
 * Represents Sort By choicebox.
 */
public class SortByChoiceBox extends ChoiceBox<SortBy> {

    /**
     * Initializes and creates choicebox.
     */
    public SortByChoiceBox(){
        super();
        this.setUp();
        this.createDesign();
    }

    /**
     * Setups choicebox.
     */
    private void setUp(){
        this.getItems().setAll(SortBy.values());
        this.getSelectionModel().selectFirst();
    }

    /**
     * Creates choicebox´s design.
     */
    private void createDesign(){
        this.setPadding(new Insets(2.5, 0, 0, 0));
        this.setStyle("-fx-background-color: #f8f8f8; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
    }

}
