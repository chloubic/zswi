package graphics.sections.components.filters.choiceboxes;

import graphics.sections.components.filters.SortOrder;
import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;

/**
 * Represents Sort Order choicebox.
 */
public class SortOrderChoiceBox extends ChoiceBox<SortOrder> {

    /**
     * Initializes and creates choicebox.
     */
    public SortOrderChoiceBox(){
        super();
        this.setUp();
        this.createDesign();
    }

    /**
     * Setups choicebox.
     */
    private void setUp(){
        this.getItems().setAll(SortOrder.values());
        this.getSelectionModel().selectFirst();
    }

    /**
     * Creates choicebox´s design.
     */
    private void createDesign(){
        this.setPadding(new Insets(2.5, 0, 0, 0));
        this.setStyle("-fx-background-color: #f8f8f8; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
    }

}
