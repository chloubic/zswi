package graphics.sections.components.filters;

/**
 * Represents items of {@link graphics.sections.components.filters.choiceboxes.SortOrderChoiceBox}.
 */
public enum SortOrder {

    /**
     * SortOrder items.
     */
    ASCENDING("Ascending"), DESCENDING("Descending");

    /**
     * Declaring private variable for getting values
     */
    private String action;

    /**
     * Getter method.
     * @return Action name
     */
    public String toString() { return this.action; }

    /**
     * Creates action.
     * @param action Action name
     */
    SortOrder(String action) { this.action = action; }
}
