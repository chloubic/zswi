package graphics.sections.components.filters;

/**
 * Represents items of {@link graphics.sections.components.filters.choiceboxes.SortByChoiceBox}.
 */
public enum SortBy {

    /**
     * SortBy item.
     */
    ALPHABETICALLY("Alphabetically");

    /**
     * Declaring private variable for getting values.
     */
    private String action;

    /**
     * Getter method.
     * @return Action name
     */
    public String toString() { return this.action; }

    /**
     * Creates action.
     * @param action Action name
     */
    SortBy(String action) { this.action = action; }
}