package graphics.sections.components.buttons;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents additional panel close button.
 */
public class AddPanCloseButton extends Button {

    /**
     * Initializes and creates close button.
     * @param buttonLabel Close button label
     */
    public AddPanCloseButton(String buttonLabel){
        super(buttonLabel);
        this.setUp();
        this.createDesign();
    }

    /**
     * Initializes close button.
     */
    private void setUp() {
        Text closeButtonIcon = GlyphsDude.createIcon(FontAwesomeIcon.CLOSE, "14px");
        this.setGraphic(closeButtonIcon);
    }

    /**
     * Creates close button design.
     */
    private void createDesign() {
        this.setStyle("-fx-background-color: transparent; -fx-padding: 7.5px 15px 7.5px 15px; -fx-font-weight: 900; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.setTextFill(Color.web("#787878"));
        ((Text) this.getGraphic()).setFill(Color.web("#787878"));
    }

}
