package graphics.sections.components.buttons;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents details button.
 */
public class DetailsButton extends Button {

    /**
     * Initializes and creates button.
     * @param buttonLabel Button label
     */
    public DetailsButton(String buttonLabel){
        super(buttonLabel);
        this.setUp();
        this.createDesign();
    }

    /**
     * Creates button´s design.
     */
    private void createDesign() {
        this.setStyle("-fx-font-weight: bold; -fx-background-color: transparent; -fx-text-fill: #375e97; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        ((Text) this.getGraphic()).setFill(Color.web("#375e97"));
    }

    /**
     * Setups button.
     */
    private void setUp() {
        Text showDetailsIcon = GlyphsDude.createIcon(FontAwesomeIcon.EYE, "12px");
        this.setGraphic(showDetailsIcon);
    }

}
