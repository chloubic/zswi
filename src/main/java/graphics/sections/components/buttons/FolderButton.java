package graphics.sections.components.buttons;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents folder button.
 */
public class FolderButton extends Button {

    /**
     * Initializes and creates button.
     * @param buttonLabel Button label
     */
    public FolderButton(String buttonLabel){
        super(buttonLabel);
        this.setUp();
        this.createDesign();
    }

    /**
     * Creates button´s design.
     */
    private void createDesign() {
        this.setStyle("-fx-font-weight: bold; -fx-background-color: transparent; -fx-text-fill: #9b9b9b; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        ((Text) this.getGraphic()).setFill(Color.web("#9b9b9b"));
    }

    /**
     * Setups button.
     */
    private void setUp() {
        Text showInFolderIcon = GlyphsDude.createIcon(FontAwesomeIcon.FOLDER, "12px");
        this.setGraphic(showInFolderIcon);
    }

}
