package graphics.sections.components.buttons;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents remove/delete button.
 */
public class RemoveButton extends Button {

    /**
     * Initializes and creates button.
     * @param buttonLabel Button label
     */
    public RemoveButton(String buttonLabel){
        super(buttonLabel);
        this.setUp();
        this.createDesign();
    }

    /**
     * Creates button´s design.
     */
    private void createDesign() {
        this.setStyle("-fx-font-weight: bold; -fx-background-color: transparent; -fx-text-fill: red; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        ((Text) this.getGraphic()).setFill(Color.RED);
    }

    /**
     * Setups button.
     */
    private void setUp() {
        Text deleteIcon = GlyphsDude.createIcon(FontAwesomeIcon.TRASH, "12px");
        this.setGraphic(deleteIcon);
    }

}
