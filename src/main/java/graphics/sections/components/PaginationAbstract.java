package graphics.sections.components;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Class represents abstract class of pagination.
 */
public abstract class PaginationAbstract extends HBox {

    /**
     * Initial page value.
     */
    private final String INITIAL_PAGE = "1";

    /**
     * Actual page.
     */
    protected int actualPage = 1;

    /**
     * Field of actual page.
     */
    protected TextField pageInput;

    /**
     * Label for total page counter.
     */
    protected Label pageCount;

    /**
     * Next page button.
     */
    protected Button nextPage;

    /**
     * Previous page button.
     */
    protected Button previousPage;

    /**
     * Initializes and creates pagination.
     */
    public PaginationAbstract(){
        super();
        this.initialize();
        this.setUp();
        this.initializeEvents();
        this.createButtonsDesign();
        this.createFieldsDesign();
    }

    /**
     * Setups pagination.
     */
    private void setUp() {
        this.setStyle("-fx-background-color: #f8f8f8; -fx-padding: 10px");
        this.setAlignment(Pos.CENTER);
        this.setSpacing(30);

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(this.pageInput, this.pageCount);

        this.previousPage.setDisable(true);

        this.getChildren().addAll(
                this.previousPage,
                hBox,
                this.nextPage
        );
    }

    /**
     * Creates field design.
     */
    private void createFieldsDesign(){
        this.pageInput.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.pageInput.setPrefWidth(35);
        this.pageInput.setAlignment(Pos.BASELINE_RIGHT);
    }

    /**
     * Creates button design.
     */
    private void createButtonsDesign(){
        Text prevPageIcon = GlyphsDude.createIcon(FontAwesomeIcon.CHEVRON_LEFT, "10px");
        this.previousPage.setGraphic(prevPageIcon);
        ((Text) this.previousPage.getGraphic()).setFill(Color.web("#375E97"));
        this.previousPage.setTextFill(Color.web("#375E97"));
        this.previousPage.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");

        Text nextPageIcon = GlyphsDude.createIcon(FontAwesomeIcon.CHEVRON_RIGHT, "10px");
        this.nextPage.setGraphic(nextPageIcon);
        this.nextPage.setContentDisplay(ContentDisplay.RIGHT);
        ((Text) this.nextPage.getGraphic()).setFill(Color.web("#375E97"));
        this.nextPage.setTextFill(Color.web("#375E97"));
        this.nextPage.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
    }

    /**
     * Initializes pagination.
     */
    private void initialize() {
        this.pageInput = new TextField(this.INITIAL_PAGE);
        this.pageCount = new Label("of 1");
        this.nextPage = new Button("Next page");
        this.previousPage = new Button("Previous page");
    }

    /**
     * Sets page count.
     * @param pageCount Page count
     */
    public void setPageCount(int pageCount){
        this.pageCount.setText(String.format("of %d", Math.max(pageCount, 1)));
    }

    /**
     * Sets actual page.
     * @param page Actual page
     */
    public void setPageInput(int page){
        this.actualPage = page;
        this.pageInput.setText(String.format("%d", this.actualPage));
    }

    /**
     * Method for showing next page.
     */
    public void triggerNextPage(){
        this.setPageInput(this.actualPage + 1);
    }

    /**
     * Method for showing previous page.
     */
    public void triggerPreviousPage(){
        this.setPageInput(this.actualPage - 1);
    }

    /**
     * Initializes events.
     */
    public void initializeEvents(){
        this.nextPage.setOnMouseClicked(e -> this.triggerNextPage());
        this.previousPage.setOnMouseClicked(e -> this.triggerPreviousPage());
    }

    /**
     * Returns actual page.
     * @return Actual page
     */
    public int getActualPage(){ return this.actualPage; }

    /**
     * Returns actual page input.
     * @return Actual page input
     */
    public TextField getPageInput(){ return this.pageInput; }

}
