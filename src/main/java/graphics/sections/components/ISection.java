package graphics.sections.components;

import graphics.menu.MainMenu;

/**
 * Interface represents applications sections/scenes.
 */
public interface ISection {

    /**
     * Method for clearing additional panels.
     * @param mainMenu Main app menu
     */
    void clear(MainMenu mainMenu);

    /**
     * Method for setting additional panels.
     * @param mainMenu Main app menu
     */
    void set(MainMenu mainMenu);

    /**
     * Method for handling width change events.
     * @param newWidth New window width
     */
    void windowWidthChange(double newWidth);

    /**
     * Method for handling height change events.
     * @param newHeight New window height
     */
    void windowHeightChange(double newHeight);

    /**
     * Method that triggers page refresh.
     */
    void triggerPageRefresh();

}
