package graphics.sections.components.dashboard;

import core.VersionedLocalPackage;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.sections.Dashboard;
import graphics.sections.components.buttons.DetailsButton;
import graphics.sections.components.buttons.FolderButton;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents package on dashboard.
 */
public class Package extends GridPane {

    /**
     * Maximal width of package.
     * Used for calculation of sizes.
     */
    public static final double MAX_WIDTH = 300;

    /**
     * Instance of dashboard scene.
     */
    private Dashboard dashboard;

    /**
     * Package name.
     */
    private Label artifactName;

    /**
     * Package group ID.
     */
    private Label artifactPackage;

    /**
     * Package artifact ID.
     */
    private Label artifactId;

    /**
     * Show details button.
     */
    private DetailsButton showDetailsButton;

    /**
     * Show in folder button.
     */
    private FolderButton showInFolderButton;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * Versioned package.
     */
    private VersionedLocalPackage versionedLocalPackage;

    /**
     * Initializes and creates package.
     * @param dashboard Instance of dashboard
     * @param versionedLocalPackage Instance of versioned local package
     */
    public Package(Dashboard dashboard, VersionedLocalPackage versionedLocalPackage){
        super();
        this.dashboard = dashboard;
        this.versionedLocalPackage = versionedLocalPackage;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Setups package.
     */
    private void setUp() {
        this.createGeneralDesign();
        this.createLabelsDesign();
        HBox hBox = new HBox();
        hBox.setSpacing(20);
        hBox.setPadding(new Insets(10, 0, 0, 0));
        hBox.getChildren().addAll(this.showDetailsButton, this.showInFolderButton);
        this.vBox.getChildren().add(hBox);
    }

    /**
     * Creates general layout design.
     */
    private void createGeneralDesign(){
        this.setVgap(1);
        this.setHgap(1);
        this.setHeight(150);
        this.setMaxHeight(150);
        vBox.setMaxHeight(150);
        vBox.setPadding(new Insets(15, 20, 15, 20));
        vBox.setSpacing(5);
        this.add(vBox, 0, 0);
        DropShadow shadow = new DropShadow(4, Color.rgb(150, 150, 150, .5));
        this.setStyle("-fx-background-color: white; -fx-background-radius: 10px; -fx-font-family: Verdana, Arial, sans-serif");
        this.setEffect(shadow);
        GridPane.setMargin(this, new Insets(15, 20, 15, 20));
    }

    /**
     * Creates label´s design.
     */
    private void createLabelsDesign(){
        this.artifactName.setStyle("-fx-font-weight: bold; -fx-font-size: 12px; -fx-font-family: Verdana, Arial, sans-serif");

        HBox hBox = new HBox();

        this.artifactPackage.setStyle("-fx-font-size: 10px; -fx-font-family: Verdana, Arial, sans-serif");
        this.artifactPackage.setTextFill(Color.web("#888888"));
        this.artifactId.setStyle("-fx-font-size: 10px; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif; -fx-padding: 1px 0 0 0");
        this.artifactId.setTextFill(Color.web("#000000"));

        hBox.getChildren().addAll(this.artifactPackage, this.artifactId);
        this.vBox.getChildren().addAll(this.artifactName, hBox);
    }

    /**
     * Initializes package.
     */
    private void initialize() {
        vBox = new VBox();
        artifactName = new Label(this.versionedLocalPackage.getName());
        Text artifactPackageIcon = GlyphsDude.createIcon(FontAwesomeIcon.CUBES, "12px");
        artifactPackage = new Label(this.versionedLocalPackage.getGroupId());
        artifactPackage.setGraphic(artifactPackageIcon);
        artifactId = new Label(String.format(".%s", this.getVersionedLocalPackage().getVersions().get(0).getArtifactId()));
        this.showDetailsButton = new DetailsButton("Details");
        this.showInFolderButton = new FolderButton("Show in Folder");
    }

    /**
     * Initializes events of buttons.
     */
    private void initializeEvents(){
        this.showDetailsButton.setOnMouseClicked(e -> {
            this.dashboard.openPackageDetails(this);
            this.dashboard.getWindow().fireResizeEvent();
        });
        this.showInFolderButton.setOnMouseClicked(e -> this.versionedLocalPackage.showInExplorer());
    }

    /**
     * Width change event recalculator.
     */
    public void widthChangeEventHandler(){
        double packagesPerRow = Math.floor((this.dashboard.getWindow().getStage().getWidth() - 90) / (MAX_WIDTH)) - 1;
        this.setPrefWidth((this.dashboard.getWindow().getStage().getWidth() - 90) / packagesPerRow - 30);
        this.setMinWidth((this.dashboard.getWindow().getStage().getWidth() - 90) / packagesPerRow - 30);
        this.setMaxWidth((this.dashboard.getWindow().getStage().getWidth() - 90) / packagesPerRow - 30);
    }

    /**
     * Returns versioned packages.
     * @return Versioned packages
     */
    public VersionedLocalPackage getVersionedLocalPackage(){ return this.versionedLocalPackage; }

}
