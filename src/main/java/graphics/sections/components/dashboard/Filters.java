package graphics.sections.components.dashboard;

import core.SearchLimiter;
import core.VersionedLocalPackage;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.menu.IAdditionalPanel;
import graphics.sections.Dashboard;
import graphics.sections.components.filters.choiceboxes.SortByChoiceBox;
import graphics.sections.components.filters.choiceboxes.SortOrderChoiceBox;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import java.util.List;

/**
 * Represents filters of dashboard scene.
 */
public class Filters extends HBox implements IAdditionalPanel {

    /**
     * Instance of dashboard.
     */
    private Dashboard dashboard;

    /**
     * Structure design layout.
     */
    private GridPane gridPane;

    /**
     * Sort By choice box instance.
     */
    private SortByChoiceBox sortBy;

    /**
     * Sort Order choice box instance.
     */
    private SortOrderChoiceBox sortOrder;

    /**
     * Instance of search field.
     */
    private TextField searchField;

    /**
     * Instance of "check for updates" button.
     */
    private Button checkForUpdates;

    /**
     * Choicebox for "search by".
     */
    private ChoiceBox<SearchLimiter> searchChoiceBox;

    /**
     * Button for erasing search input.
     */
    private Button eraseSearchInput;

    /**
     * Initializes and creates filter panel.
     * @param dashboard Instance of dashboard
     */
    public Filters(Dashboard dashboard){
        super();
        this.dashboard = dashboard;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Initializes filter layout.
     */
    private void initialize(){
        this.gridPane = new GridPane();
        this.sortBy = new SortByChoiceBox();
        this.sortOrder = new SortOrderChoiceBox();
        this.searchField = new TextField();
        this.checkForUpdates = new Button("Refresh repository");
        this.eraseSearchInput = new Button();
        this.searchChoiceBox = new ChoiceBox<>();
        this.searchChoiceBox.getItems().setAll(SearchLimiter.values());
        this.searchChoiceBox.getSelectionModel().selectFirst();
    }

    /**
     * Setups filter layout.
     */
    private void setUp(){
        this.setStyle("-fx-background-color: #f8f8f8; -fx-padding: 10px; -fx-font-family: Verdana, Arial, sans-serif");

        HBox filtersHBox = new HBox();

        filtersHBox.setSpacing(10);
        Label sortLabel = GlyphsDude.createIconLabel(FontAwesomeIcon.SORT, "", "18px", "14px", ContentDisplay.LEFT);
        ((Text) sortLabel.getGraphic()).setFill(Color.web("#375E97"));
        sortLabel.setPadding(new Insets(5, 0, 0, 20));

        filtersHBox.getChildren().addAll(sortLabel, this.sortOrder, this.sortBy);

        HBox hBoxControls = new HBox();
        hBoxControls.setSpacing(20);

        Text openNewProjectIcon = GlyphsDude.createIcon(FontAwesomeIcon.UNDO, "14px");
        this.checkForUpdates.setGraphic(openNewProjectIcon);
        this.checkForUpdates.setPrefWidth(150);
        this.checkForUpdates.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-padding: 5px 0 0 0; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.checkForUpdates.setTextFill(Color.web("#375E97"));
        openNewProjectIcon.setFill(Color.web("#375E97"));

        HBox searchHBox = new HBox();

        Label searchFieldLabel = GlyphsDude.createIconLabel(FontAwesomeIcon.SEARCH, "", "18px", "14px", ContentDisplay.LEFT);
        searchFieldLabel.setTextFill(Color.web("#375E97"));
        searchFieldLabel.setStyle("-fx-padding: 2.5 5 0 5");
        ((Text) searchFieldLabel.getGraphic()).setFill(Color.web("#375E97"));
        this.searchField.setPromptText("Search for package");
        this.searchField.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-text-fill: #375E97; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.searchField.setPrefWidth(250);

        Text eraseSearchInputIcon = GlyphsDude.createIcon(FontAwesomeIcon.CLOSE, "16px");
        this.eraseSearchInput.setGraphic(eraseSearchInputIcon);
        ((Text) eraseSearchInput.getGraphic()).setFill(Color.web("#ff0000"));
        this.eraseSearchInput.setStyle("-fx-background-color: transparent");
        this.eraseSearchInput.setDisable(true);

        searchHBox.getChildren().addAll(this.searchChoiceBox, searchFieldLabel, this.searchField, this.eraseSearchInput);

        hBoxControls.getChildren().addAll(this.checkForUpdates, searchHBox);

        this.searchChoiceBox.setStyle("-fx-font-weight: bold; -fx-background-color: transparent; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.searchChoiceBox.setPrefWidth(85);

        this.gridPane.add(filtersHBox, 0, 0);
        this.gridPane.add(hBoxControls, 1, 0);

        ColumnConstraints largeColumn = new ColumnConstraints();
        ColumnConstraints smallColumn = new ColumnConstraints();
        largeColumn.setHgrow(Priority.ALWAYS);
        largeColumn.setHalignment(HPos.LEFT);
        smallColumn.setHgrow(Priority.NEVER);
        smallColumn.setHalignment(HPos.RIGHT);
        smallColumn.setPrefWidth(550);
        this.gridPane.setPrefWidth(Integer.MAX_VALUE);
        this.gridPane.getColumnConstraints().addAll(largeColumn, smallColumn);

        this.getChildren().add(this.gridPane);
    }

    /**
     * Initializes events.
     */
    private void initializeEvents(){
        this.searchField.setOnAction(e -> {
            List<VersionedLocalPackage> versionedLocalPackageList = this.dashboard.getWindow().getLocalRepository().searchByName(this.searchField.getText(), this.searchChoiceBox.getSelectionModel().getSelectedItem());
            this.dashboard.displaySearchedPackages(versionedLocalPackageList);
        });
        this.searchField.textProperty().addListener((obs, oldVal, newVal) -> this.eraseSearchInput.setDisable(newVal.equals("")));
        this.eraseSearchInput.setOnMouseClicked(e -> {
            this.searchField.setText("");
            this.searchField.fireEvent(new KeyEvent(KeyEvent.KEY_PRESSED, "", "", KeyCode.ENTER, false, false, false, false));
        });
        this.searchChoiceBox.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends SearchLimiter> observable, SearchLimiter oldValue, SearchLimiter newValue) -> this.searchField.fireEvent(new KeyEvent(KeyEvent.KEY_PRESSED, "", "", KeyCode.ENTER, false, false, false, false)));
        this.sortOrder.getSelectionModel()
                .selectedItemProperty()
                .addListener((a, b, c) -> {
                    this.dashboard.sortRevers();
                    this.dashboard.triggerPageRefresh();
                });
        this.checkForUpdates.setOnMouseClicked(e -> this.dashboard.getWindow().refreshRepository());
    }

    /**
     * Returns sort order type.
     * @return Sort order type
     */
    public SortOrderChoiceBox getSortOrder(){ return this.sortOrder; }

}
