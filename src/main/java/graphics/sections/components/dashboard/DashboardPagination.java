package graphics.sections.components.dashboard;

import graphics.sections.Dashboard;
import graphics.sections.components.PaginationAbstract;
import org.codehaus.plexus.util.StringUtils;

/**
 * Class represents pagination of dashboard section.
 */
public class DashboardPagination extends PaginationAbstract {

    /**
     * Instance of dashboard.
      */
    private Dashboard dashboard;

    /**
     * Initializes and creates pagination.
     * @param dashboard Instance of dashboard
     */
    public DashboardPagination(Dashboard dashboard){
        super();
        this.dashboard = dashboard;
    }

    /**
     * Method for showing next page.
     */
    public void triggerNextPage(){
        super.triggerNextPage();
        this.dashboard.triggerPageRefresh();
        this.checkButtons();
    }

    /**
     * Method for showing previous page.
     */
    public void triggerPreviousPage(){
        super.triggerPreviousPage();
        this.dashboard.triggerPageRefresh();
        this.checkButtons();
    }

    /**
     * Initializes events.
     */
    public void initializeEvents(){
        super.initializeEvents();
        this.pageInput.setOnAction(e -> {
            String page = this.pageInput.getText();
            if(!StringUtils.isNumeric(page) || page.equals("")){
                page = String.format("%d", this.actualPage);
                this.dashboard.getWindow().getMainMenu().flashFailMessage("Entered page is not numeric value");
            }
            int pageNumber = Integer.parseInt(page);
            if(pageNumber >= ((int)Math.ceil(this.dashboard.getAvailablePackages().size() / (double)this.dashboard.getWindow().getConfigFile().getPostPerPage()))) {
                pageNumber = ((int) Math.ceil(this.dashboard.getAvailablePackages().size() / (double) this.dashboard.getWindow().getConfigFile().getPostPerPage()));
                this.dashboard.getWindow().getMainMenu().flashFailMessage("Entered page is greater than max value");
            }
            else if(pageNumber < 1) {
                pageNumber = 1;
                this.dashboard.getWindow().getMainMenu().flashFailMessage("Entered page is smaller than min value");
            }
            this.actualPage = Math.max(pageNumber, 1);
            this.pageInput.setText(String.format("%d", this.actualPage));
            this.dashboard.triggerPageRefresh();
            this.checkButtons();
        });
    }

    /**
     * Checks buttons availability.
     */
    public void checkButtons(){
        if(this.actualPage <= 1)
            this.previousPage.setDisable(true);
        else
            this.previousPage.setDisable(false);
        if(this.actualPage >= ((int)Math.ceil(this.dashboard.getAvailablePackages().size() / (double)this.dashboard.getWindow().getConfigFile().getPostPerPage())))
            this.nextPage.setDisable(true);
        else
            this.nextPage.setDisable(false);
    }
}
