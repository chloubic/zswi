package graphics.sections.components.packagedetails;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.sections.PackageDetails;
import graphics.menu.IAdditionalPanel;
import graphics.sections.components.buttons.AddPanCloseButton;
import graphics.sections.components.buttons.FolderButton;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents panel of package details scene.
 */
public class Panel extends VBox implements IAdditionalPanel {

    /**
     * Instance of package details scene.
     */
    private PackageDetails packageDetails;

    /**
     * Layout for elements.
     */
    private GridPane projectInfo;

    /**
     * Package name.
     */
    private Label packageName;

    /**
     * Package group ID.
     */
    private Label packageGroup;

    /**
     * Package artifact ID.
     */
    private Label artifactId;

    /**
     * Show in folder button.
     */
    private FolderButton showPackageInFolder;

    /**
     * "Check for updates" button.
     */
    private Button checkForUpdates;

    /**
     * Package details close button.
     */
    private AddPanCloseButton closeButton;

    /**
     * Initializes and creates panel.
     * @param packageDetails Instance of package details scene
     */
    public Panel(PackageDetails packageDetails){
        super();
        this.packageDetails = packageDetails;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Initializes panel.
     */
    private void initialize(){
        this.projectInfo = new GridPane();
        initializeLabels();
        initializeButtons();
    }

    /**
     * Initializes labels.
     */
    private void initializeLabels(){
        this.packageName = new Label(this.packageDetails.getPackage().getVersionedLocalPackage().getName());
        Text artifactPackageIcon = GlyphsDude.createIcon(FontAwesomeIcon.CUBES, "14px");
        packageGroup = new Label(this.packageDetails.getPackage().getVersionedLocalPackage().getGroupId());
        packageGroup.setGraphic(artifactPackageIcon);
        artifactId = new Label(String.format(".%s", this.packageDetails.getPackage().getVersionedLocalPackage().getVersions().get(0).getArtifactId()));
    }

    /**
     * Initializes buttons.
     */
    private void initializeButtons(){
        this.checkForUpdates = new Button("Refresh repository");
        this.closeButton = new AddPanCloseButton("Close package");
        this.showPackageInFolder = new FolderButton("Folder");
    }

    /**
     * Setups panel.
     */
    private void setUp(){
        this.projectInfo.setGridLinesVisible(false);
        this.projectInfo.setPadding(new Insets(20));
        this.projectInfo.setStyle("-fx-background-color: #ffffff");
        ColumnConstraints largeColumn = new ColumnConstraints();
        ColumnConstraints smallColumn = new ColumnConstraints();
        largeColumn.setHgrow(Priority.ALWAYS);
        largeColumn.setHalignment(HPos.LEFT);
        smallColumn.setHgrow(Priority.NEVER);
        smallColumn.setHalignment(HPos.RIGHT);
        smallColumn.setPrefWidth(200);
        this.projectInfo.setPrefWidth(Integer.MAX_VALUE);
        this.projectInfo.getColumnConstraints().addAll(largeColumn, smallColumn);

        Text openNewProjectIcon = GlyphsDude.createIcon(FontAwesomeIcon.UNDO, "12px");
        this.checkForUpdates.setGraphic(openNewProjectIcon);
        this.checkForUpdates.setPrefWidth(150);
        this.checkForUpdates.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-padding: 5px 0 0 0; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.checkForUpdates.setTextFill(Color.web("#375E97"));
        openNewProjectIcon.setFill(Color.web("#375E97"));

        HBox hBox = new HBox();
        hBox.getChildren().addAll(this.packageGroup, this.artifactId);

        HBox hBoxButtons = new HBox();
        hBoxButtons.setSpacing(30);
        hBoxButtons.getChildren().addAll(this.checkForUpdates, this.showPackageInFolder);

        this.projectInfo.add(this.packageName, 0, 0);
        this.projectInfo.add(this.closeButton, 1, 0);
        this.projectInfo.add(hBox, 0, 1);
        this.projectInfo.add(hBoxButtons, 0, 2);

        createLabelsDesign();

        this.getChildren().addAll(this.projectInfo);
    }

    /**
     * Creates label´s design.
     */
    private void createLabelsDesign(){
        this.packageName.setTextFill(Color.web("#454545"));
        this.packageName.setStyle("-fx-padding: 0 0 10px 0; -fx-font-weight: 900; -fx-font-size: 20px; -fx-font-family: Verdana, Arial, sans-serif");

        this.packageGroup.setStyle("-fx-font-size: 10px; -fx-padding: 0 0 20px 0; -fx-font-family: Verdana, Arial, sans-serif");
        this.packageGroup.setTextFill(Color.web("#888888"));

        this.artifactId.setStyle("-fx-font-size: 10px; -fx-padding: 1px 0 20px 0; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif");
        this.artifactId.setTextFill(Color.web("#000000"));
    }

    /**
     * Initializes buttons events.
     */
    private void initializeEvents(){
        this.closeButton.setOnMouseClicked(e -> this.packageDetails.closeDetails());
        this.checkForUpdates.setOnMouseClicked(e -> this.packageDetails.refreshVersions());
        this.showPackageInFolder.setOnMouseClicked(e -> this.packageDetails.getPackage().getVersionedLocalPackage().showInExplorer() );
    }

}
