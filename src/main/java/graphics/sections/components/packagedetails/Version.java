package graphics.sections.components.packagedetails;

import core.LocalPackage;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.sections.PackageDetails;
import graphics.sections.components.buttons.FolderButton;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents version block of package.
 */
public class Version extends GridPane {

    /**
     * Maximal width of version.
     * Used for calculation of sizes.
     */
    public static final double MAX_WIDTH = 300;

    /**
     * Instance of package details scene.
     */
    private PackageDetails packageDetails;

    /**
     * Version number.
     */
    private Label versionNumber;

    /**
     * Class files label.
     */
    private Label classFiles;

    /**
     * Class files indicator.
     */
    private Text classFilesStatusIcon;

    /**
     * Binary files label.
     */
    private Label binaryFiles;

    /**
     * Binary files indicator.
     */
    private Text binaryFilesStatusIcon;

    /**
     * JavaDoc label.
     */
    private Label javaDoc;

    /**
     * JavaDoc indicator.
     */
    private Text javaDocStatusIcon;

    /**
     * Show in folder button.
     */
    private Button showInFolderButton;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * Instance of local package.
     */
    private LocalPackage localPackage;

    /**
     * Initializes and creates version.
     * @param packageDetails Instance of package details scene
     * @param localPackage Instance of local package
     */
    public Version(PackageDetails packageDetails, LocalPackage localPackage){
        super();
        this.packageDetails = packageDetails;
        this.localPackage = localPackage;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Setups version.
     */
    private void setUp() {
        this.createGeneralDesign();
        this.createLabelsDesign();
        HBox hBox = new HBox();
        hBox.setSpacing(20);
        hBox.setPadding(new Insets(10, 0, 0, 0));
        hBox.getChildren().addAll(this.showInFolderButton);
        this.vBox.getChildren().add(hBox);
    }

    /**
     * Creates general layout design.
     */
    private void createGeneralDesign(){
        this.setVgap(1);
        this.setHgap(1);
        this.setHeight(150);
        this.setMaxHeight(150);
        vBox.setMaxHeight(150);
        vBox.setPadding(new Insets(15, 20, 15, 20));
        vBox.setSpacing(5);
        this.add(vBox, 0, 0);
        DropShadow shadow = new DropShadow(4, Color.rgb(150, 150, 150, .5));
        this.setStyle("-fx-background-color: white; -fx-background-radius: 10px; -fx-font-family: Verdana, Arial, sans-serif");
        this.setEffect(shadow);
        GridPane.setMargin(this, new Insets(15, 20, 15, 20));
    }

    /**
     * Creates label´s design.
     */
    private void createLabelsDesign(){
        this.versionNumber.setStyle("-fx-font-weight: bold; -fx-font-size: 12px; -fx-font-family: Verdana, Arial, sans-serif");

        this.classFiles.setGraphic(this.classFilesStatusIcon);
        this.binaryFiles.setGraphic(this.binaryFilesStatusIcon);
        this.javaDoc.setGraphic(this.javaDocStatusIcon);

        this.initializeClassIcon();
        this.initializeBinaryIcon();
        this.initializeJavaDocIcon();

        this.vBox.getChildren().addAll(this.versionNumber, this.classFiles, this.binaryFiles, this.javaDoc);
    }

    /**
     * Initializes javadoc icon.
     */
    private void initializeJavaDocIcon() {
        if(this.localPackage.isJavadoc())
            this.javaDocStatusIcon.setFill(Color.web("#00a90c"));
        else
            this.javaDocStatusIcon.setFill(Color.web("#ff0000"));
    }

    /**
     * Initializes source icon.
     */
    private void initializeBinaryIcon() {
        if(this.localPackage.isSource())
            this.binaryFilesStatusIcon.setFill(Color.web("#00a90c"));
        else
            this.binaryFilesStatusIcon.setFill(Color.web("#ff0000"));
    }

    /**
     * Initializes class icon.
     */
    private void initializeClassIcon() {
        if(this.localPackage.isClass())
            this.classFilesStatusIcon.setFill(Color.web("#00a90c"));
        else
            this.classFilesStatusIcon.setFill(Color.web("#ff0000"));
    }


    /**
     * Initializes version.
     */
    private void initialize() {
        this.classFiles = new Label("Class files");
        this.binaryFiles = new Label("Source files");
        this.javaDoc = new Label("JavaDoc files");

        this.classFilesStatusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "12px");
        this.binaryFilesStatusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "12px");
        this.javaDocStatusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "12px");
        vBox = new VBox();
        versionNumber = new Label(String.format("Version %s", this.localPackage.getVersion()));
        this.showInFolderButton = new FolderButton("Show in Folder");
    }

    /**
     * Initializes events.
     */
    private void initializeEvents(){
        this.showInFolderButton.setOnMouseClicked(e -> this.localPackage.showInExplorer());
    }

    /**
     * Width change event recalculator.
     */
    public void widthChangeEventHandler(){
        double versionsPerRow = Math.floor((this.packageDetails.getDashboard().getWindow().getStage().getWidth() - 90) / (MAX_WIDTH)) - 1;
        this.setPrefWidth((this.packageDetails.getDashboard().getWindow().getStage().getWidth() - 90) / versionsPerRow - 30);
        this.setMinWidth((this.packageDetails.getDashboard().getWindow().getStage().getWidth() - 90) / versionsPerRow - 30);
        this.setMaxWidth((this.packageDetails.getDashboard().getWindow().getStage().getWidth() - 90) / versionsPerRow - 30);
    }

}