package graphics.sections.components.projects;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.menu.IAdditionalPanel;
import graphics.sections.Projects;
import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.json.simple.JSONObject;

import java.io.File;
import java.util.List;

/**
 * Represents filters of projects scene.
 */
public class Filters extends HBox implements IAdditionalPanel {

    /**
     * Structure design layout.
     */
    private GridPane gridPane;

    /**
     * Instance of "Recent projects" label.
     */
    private Label recentLabel;

    /**
     * Instance of button for opening new project.
     */
    private Button openNewProject;

    /**
     * Instance of search field.
     */
    private TextField searchField;

    /**
     * Button for erasing input.
     */
    private Button eraseSearchInput;

    /**
     * Instance of project POM file chooser.
     */
    private FileChooser fileChooser;

    /**
     * Instance of projects section.
     */
    private Projects projects;

    /**
     * Initializes and creates filter panel.
     * @param projects Instance of projects
     */
    public Filters(Projects projects){
        super();
        this.projects = projects;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Initializes filter layout.
     */
    private void initialize(){
        this.gridPane = new GridPane();
        this.recentLabel = new Label("Recent projects");
        this.openNewProject = new Button("Open new");
        this.searchField = new TextField();
        this.fileChooser = new FileChooser();
        this.eraseSearchInput = new Button();
    }

    /**
     * Setups filter layout.
     */
    private void setUp(){
        this.setStyle("-fx-background-color: #f8f8f8; -fx-padding: 10px");

        this.recentLabel.setStyle("-fx-font-size: 16px; -fx-padding: 0 0 0 20px; -fx-font-weight: 700; -fx-font-family: Verdana, Arial, sans-serif");
        this.recentLabel.setTextFill(Color.web("#323232"));

        HBox hBoxControls = new HBox();
        hBoxControls.setSpacing(30);

        Text openNewProjectIcon = GlyphsDude.createIcon(FontAwesomeIcon.FOLDER, "12px");
        this.openNewProject.setGraphic(openNewProjectIcon);
        this.openNewProject.setPrefWidth(140);
        this.openNewProject.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.openNewProject.setTextFill(Color.web("#375E97"));
        openNewProjectIcon.setFill(Color.web("#375E97"));

        HBox searchHBox = new HBox();

        Label searchFieldLabel = GlyphsDude.createIconLabel(FontAwesomeIcon.SEARCH, "", "18px", "14px", ContentDisplay.LEFT);
        searchFieldLabel.setTextFill(Color.web("#375E97"));
        searchFieldLabel.setStyle("-fx-padding: 2.5 5 0 0");
        ((Text) searchFieldLabel.getGraphic()).setFill(Color.web("#375E97"));
        this.searchField.setPromptText("Search for project");
        this.searchField.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-text-fill: #375E97; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.searchField.setPrefWidth(250);

        Text eraseSearchInputIcon = GlyphsDude.createIcon(FontAwesomeIcon.CLOSE, "16px");
        this.eraseSearchInput.setGraphic(eraseSearchInputIcon);
        ((Text) eraseSearchInput.getGraphic()).setFill(Color.web("#ff0000"));
        this.eraseSearchInput.setStyle("-fx-background-color: transparent; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.eraseSearchInput.setDisable(true);

        searchHBox.getChildren().addAll(searchFieldLabel, this.searchField, this.eraseSearchInput);

        hBoxControls.getChildren().addAll(this.openNewProject, searchHBox);

        this.gridPane.add(this.recentLabel, 0, 0);
        this.gridPane.add(hBoxControls, 1, 0);

        ColumnConstraints largeColumn = new ColumnConstraints();
        ColumnConstraints smallColumn = new ColumnConstraints();
        largeColumn.setHgrow(Priority.ALWAYS);
        largeColumn.setHalignment(HPos.LEFT);
        smallColumn.setHgrow(Priority.NEVER);
        smallColumn.setHalignment(HPos.RIGHT);
        smallColumn.setPrefWidth(400);
        this.gridPane.setPrefWidth(Integer.MAX_VALUE);
        this.gridPane.getColumnConstraints().addAll(largeColumn, smallColumn);

        this.getChildren().add(this.gridPane);
    }

    /**
     * Initializes events.
     */
    private void initializeEvents(){
        this.openNewProject.setOnMouseClicked(e -> {
            File pomFile = this.fileChooser.showOpenDialog(this.projects.getWindow().getStage());
            if(pomFile != null) {
                this.projects.getWindow().getConfigFile().writeNewProject(
                        pomFile.getParentFile().getName(),
                        pomFile.getAbsolutePath()
                );
                List<JSONObject> projects = this.projects.getWindow().getConfigFile().getProjectsList();
                this.projects.getProjectsAvailable().add(0,
                        new Project(
                                this.projects,
                                projects.get(projects.size() - 1)
                        )
                );
                this.projects.getProjectsPagination().setPageCount((int)Math.ceil(this.projects.getProjectsAvailable().size() / (double)this.projects.getWindow().getConfigFile().getPostPerPage()));
                this.projects.getProjectsPagination().setPageInput(this.projects.getProjectsPagination().getActualPage());
                this.projects.getProjectsPagination().getPageInput().fireEvent(new KeyEvent(KeyEvent.KEY_PRESSED, "", "", KeyCode.ENTER, false, false, false, false));
                this.projects.getProjectsPagination().checkButtons();
                this.projects.triggerPageRefresh();
                this.projects.getWindow().getMainMenu().flashInfoMessage("Project added");
            }
        });
        this.searchField.textProperty().addListener((obs, oldVal, newVal) -> this.eraseSearchInput.setDisable(newVal.equals("")));
        this.searchField.setOnAction(e -> {
            List<JSONObject> searchedProjects = this.projects.getWindow().getConfigFile().getProjectsByName(this.searchField.getText());
            this.projects.displaySearchedPackages(searchedProjects);
            if(searchField.getText().equals("")) this.recentLabel.setText("Recent projects");
            else this.recentLabel.setText("Found projects");
        });
        this.eraseSearchInput.setOnMouseClicked(e -> {
            this.searchField.setText("");
            this.searchField.fireEvent(new KeyEvent(KeyEvent.KEY_PRESSED, "", "", KeyCode.ENTER, false, false, false, false));
        });
    }

}
