package graphics.sections.components.projects;

import core.FileOperator;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.sections.Projects;
import graphics.sections.components.buttons.DetailsButton;
import graphics.sections.components.buttons.FolderButton;
import graphics.sections.components.buttons.RemoveButton;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import org.json.simple.JSONObject;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents project of projects scene.
 */
public class Project extends GridPane {

    /**
     * Maximal width of project.
     * Used for calculation of sizes.
     */
    public static final double MAX_WIDTH = 300;

    /**
     * Instance of projects scene.
     */
    private Projects projects;

    /**
     * Projects object.
     */
    private JSONObject project;

    /**
     * Project name label.
     */
    private Label projectName;

    /**
     * Last usage date label.
     */
    private Label lastUsage;

    /**
     * Show details button.
     */
    private DetailsButton openButton;

    /**
     * Remove button.
     */
    private RemoveButton removeButton;

    /**
     * Show in folder button.
     */
    private FolderButton folderButton;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * Initializes and creates project.
     * @param projects Instance of projects scene
     * @param project Project object
     */
    public Project(Projects projects, JSONObject project){
        super();
        this.projects = projects;
        this.project = project;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Setups project.
     */
    private void setUp() {
        this.createGeneralDesign();
        this.createLabelsDesign();
        HBox hBox = new HBox();
        hBox.setSpacing(20);
        hBox.setPadding(new Insets(10, 0, 0, 0));
        if(isStillAvailable())
            hBox.getChildren().add(this.openButton);
        hBox.getChildren().add(this.removeButton);
        if(isStillAvailable())
            hBox.getChildren().add(this.folderButton);
        this.vBox.getChildren().add(hBox);
    }

    /**
     * Creates general layout design.
     */
    private void createGeneralDesign(){
        this.setVgap(1);
        this.setHgap(1);
        this.setHeight(150);
        this.setMaxHeight(150);
        vBox.setMaxHeight(150);
        vBox.setPadding(new Insets(15, 20, 15, 20));
        vBox.setSpacing(5);
        this.add(vBox, 0, 0);
        DropShadow shadow = new DropShadow(4, Color.rgb(150, 150, 150, .5));
        this.setStyle("-fx-background-color: white; -fx-background-radius: 10px; -fx-font-family: Verdana, Arial, sans-serif");
        this.setEffect(shadow);
        GridPane.setMargin(this, new Insets(15, 20, 15, 20));
    }

    /**
     * Creates label´s design.
     */
    private void createLabelsDesign(){
        this.projectName.setStyle("-fx-font-weight: bold; -fx-font-size: 14px; -fx-font-family: Verdana, Arial, sans-serif");
        this.lastUsage.setStyle("-fx-font-size: 11px; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif");
        this.vBox.getChildren().addAll(this.projectName, this.lastUsage);
    }

    /**
     * Initializes project.
     */
    private void initialize() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm a");
        vBox = new VBox();
        Text statusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "16px");
        this.projectName = new Label(this.project.get("name").toString());
        this.projectName.setGraphic(statusIcon);

        if(isStillAvailable()) statusIcon.setFill(Color.web("#00a90c"));
        else statusIcon.setFill(Color.web("#ff0000"));

        Text lastUsageIcon = GlyphsDude.createIcon(FontAwesomeIcon.CLOCK_ALT, "12px");
        this.lastUsage = new Label(simpleDateFormat.format(new Date(Long.parseLong(this.project.get("lastUpdate").toString()))));
        this.lastUsage.setGraphic(lastUsageIcon);

        this.openButton = new DetailsButton("Open");
        this.removeButton = new RemoveButton("Remove");
        this.folderButton = new FolderButton("Folder");
    }

    /**
     * Checks if POM is still available.
     * @return Availability of project
     */
    private boolean isStillAvailable(){
        File file = new File(this.project.get("path").toString());
        return file.exists();
    }

    /**
     * Width change event recalculator.
     */
    public void widthChangeEventHandler(){
        double projectsPerRow = Math.floor((this.projects.getWindow().getStage().getWidth() - 90) / (MAX_WIDTH)) - 1;
        this.setPrefWidth((this.projects.getWindow().getStage().getWidth() - 90) / projectsPerRow - 30);
        this.setMinWidth((this.projects.getWindow().getStage().getWidth() - 90) / projectsPerRow - 30);
        this.setMaxWidth((this.projects.getWindow().getStage().getWidth() - 90) / projectsPerRow - 30);
    }

    /**
     * Initializes events of buttons.
     */
    private void initializeEvents(){
        this.openButton.setOnMouseClicked(e -> {
            this.projects.openProjectDetails(
                    this,
                    FileOperator.loadProject(Paths.get(this.project.get("path").toString()))
            );
            this.reNewDate();
        });
        this.removeButton.setOnMouseClicked(e -> {
            this.projects.getWindow().getConfigFile().removeProject(this.project);
            this.projects.getProjectsAvailable().remove(this);
            this.projects.getProjectsPagination().getPageInput().fireEvent(new KeyEvent(KeyEvent.KEY_PRESSED, "", "", KeyCode.ENTER, false, false, false, false));
            this.projects.refreshPagination();
            this.projects.triggerPageRefresh();
            this.projects.getWindow().getMainMenu().flashInfoMessage("Project removed");
        });
        this.folderButton.setOnMouseClicked(e -> this.showInExplorer());
    }

    /**
     * Changes date of project.
     * @return Date
     */
    public String reNewDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm a");
        String newDate = simpleDateFormat.format(new Date(Long.parseLong(this.project.get("lastUpdate").toString())));
        this.lastUsage.setText(newDate);
        return newDate;
    }

    /**
     * Shows project in default explorer.
     */
    public void showInExplorer(){
        if (Desktop.isDesktopSupported()) {
            new Thread(() -> {
                try {
                    Desktop.getDesktop().open((new File(this.project.get("path").toString())).getParentFile().getAbsoluteFile());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }).start();
        }
    }

    /**
     * Returns project object.
     * @return Project object
     */
    public JSONObject getProject() { return project; }
}
