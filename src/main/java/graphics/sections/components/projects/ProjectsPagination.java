package graphics.sections.components.projects;

import graphics.sections.Projects;
import graphics.sections.components.PaginationAbstract;
import org.codehaus.plexus.util.StringUtils;

/**
 * Class represents pagination of projects section.
 */
public class ProjectsPagination extends PaginationAbstract {

    /**
     * Instance of projects
     */
    private Projects projects;

    /**
     * Initializes and creates pagination.
     * @param projects Instance of projects
     */
    public ProjectsPagination(Projects projects){
        super();
        this.projects = projects;
    }

    /**
     * Method for showing next page.
     */
    public void triggerNextPage(){
        super.triggerNextPage();
        this.projects.triggerPageRefresh();
        this.checkButtons();
    }

    /**
     * Method for showing previous page.
     */
    public void triggerPreviousPage(){
        super.triggerPreviousPage();
        this.projects.triggerPageRefresh();
        this.checkButtons();
    }

    /**
     * Initializes events.
     */
    public void initializeEvents(){
        super.initializeEvents();
        this.pageInput.setOnAction(e -> {
            String page = this.pageInput.getText();
            if(!StringUtils.isNumeric(page) || page.equals("")){
                page = String.format("%d", this.actualPage);
                this.projects.getWindow().getMainMenu().flashFailMessage("Entered page is not numeric value");
            }
            int pageNumber = Integer.parseInt(page);
            if(pageNumber >= ((int)Math.ceil(this.projects.getProjectsAvailable().size() / (double)this.projects.getWindow().getConfigFile().getPostPerPage()))) {
                pageNumber = ((int) Math.ceil(this.projects.getProjectsAvailable().size() / (double) this.projects.getWindow().getConfigFile().getPostPerPage()));
                this.projects.getWindow().getMainMenu().flashFailMessage("Entered page is greater than max value");
            }
            else if(pageNumber < 1) {
                pageNumber = 1;
                this.projects.getWindow().getMainMenu().flashFailMessage("Entered page is smaller than min value");
            }
            this.actualPage = Math.max(pageNumber, 1);
            this.pageInput.setText(String.format("%d", this.actualPage));
            this.projects.triggerPageRefresh();
            this.projects.refreshPagination();
            this.checkButtons();
        });
    }

    /**
     * Checks buttons availability.
     */
    public void checkButtons(){
        if(this.actualPage <= 1)
            this.previousPage.setDisable(true);
        else
            this.previousPage.setDisable(false);
        if(this.actualPage >= ((int)Math.ceil(this.projects.getProjectsAvailable().size() / (double)this.projects.getWindow().getConfigFile().getPostPerPage())))
            this.nextPage.setDisable(true);
        else
            this.nextPage.setDisable(false);
    }

}
