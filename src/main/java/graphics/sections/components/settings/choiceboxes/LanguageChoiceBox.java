package graphics.sections.components.settings.choiceboxes;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;

/**
 * Represents language changer.
 */
public class LanguageChoiceBox extends ChoiceBox<String> {

    /**
     * Initializes and creates language changer.
     */
    public LanguageChoiceBox(){
        super(FXCollections.observableArrayList("English"));
        this.setUp();
        this.createDesign();
    }

    /**
     * Setups changer.
     */
    private void setUp(){ this.getSelectionModel().selectFirst(); }

    /**
     * Creates changer design.
     */
    private void createDesign(){
        this.setPadding(new Insets(5, 0, 0, 0));
        this.setStyle("-fx-background-color: white; -fx-font-family: Verdana, Arial, sans-serif");
    }

}
