package graphics.sections.components.settings.labels;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Represents version label.
 */
public class VersionLabel extends Label {

    /**
     * Initializes and creates version label.
     */
    public VersionLabel(){
        super("Version 1.0.0");
        this.createDesign();
    }

    /**
     * Creates version label design.
     */
    private void createDesign(){
        this.setTextFill(Color.web("#9B9B9B"));
        this.setFont(new Font(14.0));
        this.setPadding(new Insets(10, 0, 0, 0));
    }

}
