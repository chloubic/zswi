package graphics.sections.components.settings.labels;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Represents copyrights label.
 */
public class CopyrightsLabel extends Label {

    /**
     * Initializes and creates copyrights label.
     */
    public CopyrightsLabel(){
        super("Copyright © X-303 Crew, 2019. All rights reserved.");
        this.createDesign();
    }

    /**
     * Creates copyrights label design.
     */
    private void createDesign(){
        this.setTextFill(Color.web("#9B9B9B"));
        this.setFont(new Font(14.0));
    }

}
