package graphics.sections.components.settings.labels;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Represents settings label.
 */
public class SettingsLabel extends Label {

    /**
     * Initializes and creates settings label.
     */
    public SettingsLabel(){
        super("Settings");
        this.createDesign();
    }

    /**
     * Creates settings label design.
     */
    private void createDesign(){
        this.setTextFill(Color.web("#375E97"));
        this.setFont(new Font(36.0));
    }

}
