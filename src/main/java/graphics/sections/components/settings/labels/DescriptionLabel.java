package graphics.sections.components.settings.labels;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Represents description label.
 */
public class DescriptionLabel extends Label {

    /**
     * Initializes and creates description label.
     */
    public DescriptionLabel(){
        super("The application was created within the semester project of KIV/ZSWI.");
        this.createDesign();
    }

    /**
     * Creates description label design.
     */
    private void createDesign(){
        this.setTextFill(Color.web("#323232"));
        this.setFont(new Font(20.0));
    }

}
