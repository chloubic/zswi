package graphics.sections.components.settings.labels;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Represents language label.
 */
public class LanguageLabel extends Label {

    /**
     * Initializes and creates language label.
     */
    public LanguageLabel(){
        super("Language");
        this.createDesign();
    }

    /**
     * Creates language label design.
     */
    private void createDesign(){
        this.setTextFill(Color.web("#323232"));
        this.setFont(new Font(20.0));
    }

}
