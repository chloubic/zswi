package graphics.sections.components.settings.labels;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Represents about label.
 */
public class AboutLabel extends Label {

    /**
     * Initializes and creates about label.
     */
    public AboutLabel(){
        super("About");
        this.createDesign();
    }

    /**
     * Creates about label design.
     */
    private void createDesign(){
        this.setTextFill(Color.web("#375E97"));
        this.setFont(new Font(36.0));
        this.setPadding(new Insets(40, 0, 0, 0));
    }

}
