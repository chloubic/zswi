package graphics.sections.components.projectdetails;

import core.LocalPackage;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.sections.ProjectDetails;
import graphics.sections.components.buttons.FolderButton;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents package of project details scene.
 */
public class Package extends GridPane {

    /**
     * Maximal width of package.
     * Used for calculation of sizes.
     */
    public static final double MAX_WIDTH = 300;

    /**
     * Instance of project details scene.
     */
    private ProjectDetails projectDetails;

    /**
     * Local package instance.
     */
    private LocalPackage localPackage;

    /**
     * Package name.
     */
    private Label artifactName;

    /**
     * Package group ID.
     */
    private Label artifactPackage;

    /**
     * Package artifact ID.
     */
    private Label artifactId;

    /**
     * Package version.
     */
    private Label artifactVersion;

    /**
     * Class files label.
     */
    private Label classFiles;

    /**
     * Class files indicator.
     */
    private Text classFilesStatusIcon;

    /**
     * Binary files label.
     */
    private Label binaryFiles;

    /**
     * Binary files indicator.
     */
    private Text binaryFilesStatusIcon;

    /**
     * JavaDoc label.
     */
    private Label javaDoc;

    /**
     * JavaDoc indicator.
     */
    private Text javaDocStatusIcon;

    /**
     * Show in folder button.
     */
    private FolderButton showInFolderButton;

    /**
     * Status indicator.
     */
    private Text statusIcon;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * Initializes and creates package.
     * @param projectDetails Instance of project details scene
     * @param localPackage Instance of local package
     */
    public Package(ProjectDetails projectDetails, LocalPackage localPackage){
        super();
        this.projectDetails = projectDetails;
        this.localPackage = localPackage;
        this.initialize();
        this.setUp();
        this.initializeEvents();
        this.setMinHeight(200);
    }

    /**
     * Setups package.
     */
    private void setUp() {
        this.createGeneralDesign();
        this.createLabelsDesign();
        HBox hBox = new HBox();
        hBox.setSpacing(20);
        hBox.setPadding(new Insets(10, 0, 0, 0));
        if(this.localPackage.exists())
            hBox.getChildren().addAll(this.showInFolderButton);
        this.vBox.getChildren().add(hBox);
    }

    /**
     * Creates general layout design.
     */
    private void createGeneralDesign(){
        this.setVgap(1);
        this.setHgap(1);
        this.setHeight(150);
        this.setMaxHeight(150);
        vBox.setMaxHeight(150);
        vBox.setPadding(new Insets(15, 20, 15, 20));
        vBox.setSpacing(5);
        this.add(vBox, 0, 0);
        DropShadow shadow = new DropShadow(4, Color.rgb(150, 150, 150, .5));
        this.setStyle("-fx-background-color: white; -fx-background-radius: 10px");
        this.setEffect(shadow);
        GridPane.setMargin(this, new Insets(15, 20, 15, 20));
    }

    /**
     * Creates label´s design.
     */
    private void createLabelsDesign(){
        this.artifactName.setStyle("-fx-font-weight: bold; -fx-font-size: 12px; -fx-font-family: Verdana, Arial, sans-serif");
        this.artifactName.setGraphic(this.statusIcon);

        if(this.localPackage.exists()) this.statusIcon.setFill(Color.web("#00a90c"));
        else this.statusIcon.setFill(Color.web("#ff0000"));

        HBox hBox = new HBox();

        this.artifactPackage.setStyle("-fx-font-size: 11px; -fx-font-family: Verdana, Arial, sans-serif");
        this.artifactPackage.setTextFill(Color.web("#888888"));

        this.artifactId.setStyle("-fx-font-size: 11px; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif");
        this.artifactId.setTextFill(Color.web("#000000"));

        hBox.getChildren().addAll(this.artifactPackage, this.artifactId);

        this.artifactVersion.setStyle("-fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif");

        this.classFiles.setGraphic(this.classFilesStatusIcon);
        this.binaryFiles.setGraphic(this.binaryFilesStatusIcon);
        this.javaDoc.setGraphic(this.javaDocStatusIcon);

        this.initializeClassIcon();
        this.initializeBinaryIcon();
        this.initializeJavaDocIcon();

        this.vBox.getChildren().addAll(
                this.artifactName,
                hBox,
                this.artifactVersion,
                this.classFiles,
                this.binaryFiles,
                this.javaDoc
        );
    }

    /**
     * Initializes javadoc icon.
     */
    private void initializeJavaDocIcon() {
        if(this.localPackage.isJavadoc())
            this.javaDocStatusIcon.setFill(Color.web("#00a90c"));
        else
            this.javaDocStatusIcon.setFill(Color.web("#ff0000"));
    }

    /**
     * Initializes source icon.
     */
    private void initializeBinaryIcon() {
        if(this.localPackage.isSource())
            this.binaryFilesStatusIcon.setFill(Color.web("#00a90c"));
        else
            this.binaryFilesStatusIcon.setFill(Color.web("#ff0000"));
    }

    /**
     * Initializes class icon.
     */
    private void initializeClassIcon() {
        if(this.localPackage.isClass())
            this.classFilesStatusIcon.setFill(Color.web("#00a90c"));
        else
            this.classFilesStatusIcon.setFill(Color.web("#ff0000"));
    }

    /**
     * Initializes package.
     */
    private void initialize() {
        this.statusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "14px");
        vBox = new VBox();
        artifactName = new Label(this.localPackage.getName());
        Text artifactPackageIcon = GlyphsDude.createIcon(FontAwesomeIcon.CUBES, "14px");
        artifactPackage = new Label(this.localPackage.getGroupId());
        artifactPackage.setGraphic(artifactPackageIcon);
        artifactId = new Label(String.format(".%s", this.localPackage.getArtifactId()));
        artifactVersion = new Label(String.format("Version %s", this.localPackage.getVersion()));

        this.classFiles = new Label("Class files");
        this.binaryFiles = new Label("Source files");
        this.javaDoc = new Label("JavaDoc files");

        this.classFilesStatusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "14px");
        this.binaryFilesStatusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "14px");
        this.javaDocStatusIcon = GlyphsDude.createIcon(FontAwesomeIcon.CIRCLE, "14px");

        this.showInFolderButton = new FolderButton("Show in Folder");
    }

    /**
     * Width change event recalculator.
     */
    public void widthChangeEventHandler(){
        double packagesPerRow = Math.floor((this.projectDetails.getProjects().getWindow().getStage().getWidth() - 90) / (MAX_WIDTH)) - 1;
        this.setPrefWidth((this.projectDetails.getProjects().getWindow().getStage().getWidth() - 90) / packagesPerRow - 30);
        this.setMinWidth((this.projectDetails.getProjects().getWindow().getStage().getWidth() - 90) / packagesPerRow - 30);
        this.setMaxWidth((this.projectDetails.getProjects().getWindow().getStage().getWidth() - 90) / packagesPerRow - 30);
    }

    /**
     * Initializes events.
     */
    private void initializeEvents(){
        this.showInFolderButton.setOnMouseClicked(e -> this.localPackage.showInExplorer());
    }

}
