package graphics.sections.components.projectdetails;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.sections.ProjectDetails;
import graphics.menu.IAdditionalPanel;
import graphics.sections.components.buttons.FolderButton;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents panel of project details scene.
 */
public class Panel extends VBox implements IAdditionalPanel {

    /**
     * Instance of project details scene.
     */
    private ProjectDetails projectDetails;

    /**
     * Panel layout for elements.
     */
    private GridPane projectInfo;

    /**
     * Project name.
     */
    private Label projectName;

    /**
     * XML status label indicator.
     */
    private Label xmlStatusLabel;

    /**
     * XML status label.
     */
    private Label xmlStatus;

    /**
     * "Check for updates" button.
     */
    private Button checkUpdates;

    /**
     * Last update icon label.
     */
    private Label lastUpdate;

    /**
     * Last update date label.
     */
    private Label lastUpdateDate;

    /**
     * Project details close button.
     */
    private Button closeButton;

    /**
     * Show in folder button.
     */
    private FolderButton openInFolder;

    /**
     * Project details´s filters.
     */
    private Filters filters;

    /**
     * Initializes and creates panel.
     * @param projectDetails Instance of project details scene
     */
    public Panel(ProjectDetails projectDetails){
        super();
        this.projectDetails = projectDetails;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Initializes panel.
     */
    private void initialize(){
        this.projectInfo = new GridPane();
        this.filters = new Filters(this.projectDetails);
        initializeLabels();
        initializeButtons();
    }

    /**
     * Initializes labels.
     */
    private void initializeLabels(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm a");
        this.projectName = new Label(this.projectDetails.getProject().getProject().get("name").toString());

        Text xmlStatusIcon = GlyphsDude.createIcon(FontAwesomeIcon.LAPTOP, "16px");
        this.xmlStatusLabel = new Label("XML status:");
        this.xmlStatusLabel.setGraphic(xmlStatusIcon);

        this.xmlStatus = new Label("Attached");

        Text lastUpdateIcon = GlyphsDude.createIcon(FontAwesomeIcon.CLOCK_ALT, "16px");
        this.lastUpdate = new Label("Last update:");
        this.lastUpdate.setGraphic(lastUpdateIcon);

        this.lastUpdateDate = new Label(simpleDateFormat.format(new Date(Long.parseLong(this.projectDetails.getProject().getProject().get("lastUpdate").toString()))));
    }

    /**
     * Initializes buttons.
     */
    private void initializeButtons(){
        this.checkUpdates = new Button("Refresh repository");

        Text closeButtonIcon = GlyphsDude.createIcon(FontAwesomeIcon.CLOSE, "16px");
        this.closeButton = new Button("Close project");
        this.closeButton.setGraphic(closeButtonIcon);

        this.openInFolder = new FolderButton("Open in Folder");
    }

    /**
     * Setups panel.
     */
    private void setUp(){
        this.projectInfo.setGridLinesVisible(false);
        this.projectInfo.setPadding(new Insets(20));
        this.projectInfo.setStyle("-fx-background-color: #ffffff");
        ColumnConstraints largeColumn = new ColumnConstraints();
        ColumnConstraints smallColumn = new ColumnConstraints();
        largeColumn.setHgrow(Priority.ALWAYS);
        largeColumn.setHalignment(HPos.LEFT);
        smallColumn.setHgrow(Priority.NEVER);
        smallColumn.setHalignment(HPos.RIGHT);
        smallColumn.setPrefWidth(200);
        this.projectInfo.setPrefWidth(Integer.MAX_VALUE);
        this.projectInfo.getColumnConstraints().addAll(largeColumn, smallColumn);
        this.projectInfo.add(this.projectName, 0, 0);

        GridPane xmlStatusPane = new GridPane();
        xmlStatusPane.add(this.xmlStatusLabel, 0, 0);
        xmlStatusPane.add(this.xmlStatus, 1, 0);
        xmlStatusPane.setPadding(new Insets(0, 0, 10, 0));

        this.projectInfo.add(xmlStatusPane, 0, 1);

        HBox lastUpdatePane = new HBox();
        HBox bottomPane = new HBox();

        lastUpdatePane.getChildren().addAll(this.lastUpdate, this.lastUpdateDate);
        lastUpdatePane.setPadding(new Insets(5, 0, 0, 0));
        bottomPane.getChildren().addAll(lastUpdatePane, this.openInFolder, this.checkUpdates);
        bottomPane.setPadding(new Insets(10, 0, 0, 0));
        bottomPane.setSpacing(30);

        this.projectInfo.add(bottomPane, 0, 2);
        this.projectInfo.add(this.closeButton, 1, 0);

        createButtonsDesign();
        createLabelsDesign();

        this.getChildren().addAll(this.projectInfo, this.filters);
    }

    /**
     * Creates button´s design.
     */
    private void createButtonsDesign() {
        this.closeButton.setStyle("-fx-background-color: transparent; -fx-padding: 7.5px 15px 7.5px 15px; -fx-font-weight: 900; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.closeButton.setTextFill(Color.web("#787878"));
        ((Text) this.closeButton.getGraphic()).setFill(Color.web("#787878"));


        Text openNewProjectIcon = GlyphsDude.createIcon(FontAwesomeIcon.UNDO, "14px");
        this.checkUpdates.setGraphic(openNewProjectIcon);
        this.checkUpdates.setPrefWidth(150);
        this.checkUpdates.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-padding: 5px 0 0 0; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-size: 11px");
        this.checkUpdates.setTextFill(Color.web("#375E97"));
        openNewProjectIcon.setFill(Color.web("#375E97"));
    }

    /**
     * Creates label´s design.
     */
    private void createLabelsDesign(){
        this.projectName.setTextFill(Color.web("#454545"));
        this.projectName.setStyle("-fx-padding: 0 0 10px 0; -fx-font-weight: 900; -fx-font-size: 20px; -fx-font-family: Verdana, Arial, sans-serif");

        this.xmlStatusLabel.setTextFill(Color.web("#454545"));
        ((Text) this.xmlStatusLabel.getGraphic()).setFill(Color.web("#00a90c"));

        this.xmlStatus.setTextFill(Color.web("#00a90c"));
        this.xmlStatus.setStyle("-fx-font-weight: bold; -fx-padding: 0 0 0 10px; -fx-font-size: 12px; -fx-font-family: Verdana, Arial, sans-serif");

        this.lastUpdate.setTextFill(Color.web("#454545"));
        ((Text) this.lastUpdate.getGraphic()).setFill(Color.web("#375e97"));

        this.lastUpdateDate.setStyle("-fx-font-weight: bold; -fx-padding: 0 0 0 10px; -fx-font-size: 11px; -fx-font-family: Verdana, Arial, sans-serif");
        this.lastUpdateDate.setTextFill(Color.web("#375e97"));
    }

    /**
     * Initializes buttons events.
     */
    private void initializeEvents(){
        this.closeButton.setOnMouseClicked(e -> this.projectDetails.closeDetails());
        this.openInFolder.setOnMouseClicked(e -> this.projectDetails.getProject().showInExplorer());
        this.checkUpdates.setOnMouseClicked(e -> {
            this.projectDetails.getProject().getProject().put("lastUpdate", String.format("%d", System.currentTimeMillis()));
            this.projectDetails.getProjects().getWindow().getConfigFile().projectChanged();
            this.lastUpdateDate.setText(this.projectDetails.getProject().reNewDate());
            this.projectDetails.getProjects().getWindow().refreshRepository();
        });
    }

}
