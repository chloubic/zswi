package graphics.sections.components.projectdetails;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.menu.IAdditionalPanel;
import graphics.sections.ProjectDetails;
import graphics.sections.components.filters.choiceboxes.SortByChoiceBox;
import graphics.sections.components.filters.choiceboxes.SortOrderChoiceBox;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents filters of project details scene.
 */
public class Filters extends HBox implements IAdditionalPanel {

    /**
     * Structure design layout.
     */
    private GridPane gridPane;

    /**
     * Sort By choice box instance.
     */
    private SortByChoiceBox sortBy;

    /**
     * Sort Order choice box instance.
     */
    private SortOrderChoiceBox sortOrder;

    /**
     * Instance of project details section.
     */
    private ProjectDetails projectDetails;

    /**
     * Initializes and creates filter panel.
     * @param projectDetails Instance of project details
     */
    public Filters(ProjectDetails projectDetails){
        super();
        this.projectDetails = projectDetails;
        this.initialize();
        this.setUp();
        this.initializeEvents();
    }

    /**
     * Initializes filter layout.
     */
    private void initialize(){
        this.gridPane = new GridPane();
        this.sortBy = new SortByChoiceBox();
        this.sortOrder = new SortOrderChoiceBox();
    }

    /**
     * Setups filter layout.
     */
    private void setUp(){
        this.setStyle("-fx-background-color: #f8f8f8; -fx-padding: 10px; -fx-font-family: Verdana, Arial, sans-serif");

        HBox filtersHBox = new HBox();

        filtersHBox.setSpacing(10);
        Label sortLabel = GlyphsDude.createIconLabel(FontAwesomeIcon.SORT, "", "16px", "14px", ContentDisplay.LEFT);
        ((Text) sortLabel.getGraphic()).setFill(Color.web("#375E97"));
        sortLabel.setPadding(new Insets(5, 0, 0, 20));

        filtersHBox.getChildren().addAll(sortLabel, this.sortOrder, this.sortBy);

        this.gridPane.add(filtersHBox, 0, 0);

        ColumnConstraints largeColumn = new ColumnConstraints();
        ColumnConstraints smallColumn = new ColumnConstraints();
        largeColumn.setHgrow(Priority.ALWAYS);
        largeColumn.setHalignment(HPos.LEFT);
        smallColumn.setHgrow(Priority.NEVER);
        smallColumn.setHalignment(HPos.RIGHT);
        smallColumn.setPrefWidth(200);
        this.gridPane.setPrefWidth(Integer.MAX_VALUE);
        this.gridPane.getColumnConstraints().addAll(largeColumn, smallColumn);

        this.getChildren().add(this.gridPane);
    }

    /**
     * Initializes events.
     */
    public void initializeEvents(){
        this.sortOrder.getSelectionModel()
                .selectedItemProperty()
                .addListener((a, b, c) -> {
                    this.projectDetails.sortRevers();
                    this.projectDetails.triggerPageRefresh();
                });
    }

}
