package graphics.sections;

import core.FileOperator;
import core.LocalPackage;
import core.MyMavenProject;
import graphics.menu.MainMenu;
import graphics.sections.components.ISection;
import graphics.sections.components.packagedetails.Version;
import graphics.sections.components.projectdetails.Package;
import graphics.sections.components.projectdetails.Panel;
import graphics.sections.components.projects.Project;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents project details.
 */
public class ProjectDetails extends ScrollPane implements ISection {

    /**
     * Instance of main {@link Projects}.
     */
    private Projects projects;

    /**
     * Project for detail information.
     */
    private Project project;

    /**
     * Instance of Maven project.
     */
    private MyMavenProject myMavenProject;

    /**
     * Instance of project details panel.
     */
    private Panel panel;

    /**
     * Main layout pane.
     */
    private Pane pane;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * List of available packages.
     */
    private List<Package> packagesAvailable;

    /**
     * Packages per row variable.
     */
    private double packagesPerRow = 1;

    /**
     * Initializes and creates project details.
     * @param project Instance of project
     * @param projects Instance of projects scene
     * @param myMavenProject Instance of maven project
     */
    public ProjectDetails(Project project, Projects projects, MyMavenProject myMavenProject){
        this.projects = projects;
        this.project = project;
        this.myMavenProject = myMavenProject;
        this.initialize();
        this.setUp();
        this.initializePackages();
    }

    /**
     * Method for package initialization.
     */
    public void initializePackages(){
        for(LocalPackage localPackage : myMavenProject.getDependencies(this.projects.getWindow().getLocalRepository()))
            this.addPackage(new Package(this, localPackage));
    }

    /**
     * Initializes project details.
     */
    private void initialize(){
        pane = new Pane();
        vBox = new VBox();
        this.packagesAvailable = new ArrayList<>();
        this.panel = new Panel(this);
    }

    /**
     * Setups project details.
     */
    private void setUp(){
        HBox hBox = new HBox();
        hBox.setSpacing(30);
        vBox.getChildren().add(hBox);
        vBox.setPadding(new Insets(30, 30, 30, 30));
        vBox.setSpacing(30);
        pane.getChildren().add(vBox);
        this.setContent(pane);
        this.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pane.setStyle("-fx-background-color: #fff");
    }

    /**
     * Adds new package.
     * @param pack New package
     */
    public void addPackage(Package pack) {
        this.packagesAvailable.add(pack);
        Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
        if(hBox instanceof HBox){
            if(((HBox) hBox).getChildren().size() >= packagesPerRow){
                HBox newHbox = new HBox();
                newHbox.setSpacing(30);
                newHbox.getChildren().add(pack);
                this.vBox.getChildren().add(newHbox);
            } else{
                ((HBox) hBox).getChildren().add(pack);
            }
        } else{
            Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot add new package to the list - HBox did not found");
            alert.show();
        }
    }

    /**
     * Width change event handler and recalculator.
     * @param newWidth New window width
     */
    public void windowWidthChange(double newWidth) {
        this.pane.setPrefWidth(newWidth - 30);
        Node nodeOut = pane.getChildren().get(0);
        if(nodeOut instanceof VBox){
            double packagesPerRow = Math.floor((this.projects.getWindow().getStage().getWidth() - 90) / (Version.MAX_WIDTH)) - 1;
            if(packagesPerRow == this.packagesPerRow) {
                for(Node nodeIn:((VBox)nodeOut).getChildren()){
                    if(nodeIn instanceof HBox){
                        ((HBox) nodeIn).setPrefWidth(this.pane.getWidth() - 60);
                        for (Node hBoxNodes : ((HBox) nodeIn).getChildren()) {
                            if (hBoxNodes instanceof GridPane) {
                                ((Package) hBoxNodes).widthChangeEventHandler();
                            }
                        }
                    }
                }
            } else reorganizeElements((VBox) nodeOut, packagesPerRow);
        }
    }

    /**
     * Recalculates and reorganizes elements.
     * @param nodeOut First {@link VBox} node
     * @param count Count of packages per row
     */
    private void reorganizeElements(VBox nodeOut, double count) {
        this.packagesPerRow = count;
        nodeOut.getChildren().clear();

        HBox initialHBox = new HBox();
        initialHBox.setSpacing(30);

        this.vBox.getChildren().add(initialHBox);

        for(Package pack : this.packagesAvailable) {
            Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
            if (hBox instanceof HBox) {
                if (((HBox) hBox).getChildren().size() >= packagesPerRow) {
                    HBox newHbox = new HBox();
                    newHbox.setSpacing(30);
                    newHbox.getChildren().add(pack);
                    this.vBox.getChildren().add(newHbox);
                } else {
                    ((HBox) hBox).getChildren().add(pack);
                }
            }
        }

        this.windowWidthChange(this.projects.getWindow().getStage().getWidth());
    }

    /**
     * Sorts available packages reverselly.
     */
    public void sortRevers(){
        ArrayList<Package> reversedList = new ArrayList<>();
        for(int i = this.packagesAvailable.size() - 1; i >= 0; i--)
            reversedList.add(this.packagesAvailable.get(i));
        this.packagesAvailable = reversedList;
    }

    /**
     * Height change event handler.
     * @param newHeight New window height
     */
    public void windowHeightChange(double newHeight) {
        this.pane.setMinHeight(newHeight - 300);
    }

    /**
     * Method that triggers page refresh.
     */
    @Override
    public void triggerPageRefresh() { this.reorganizeElements(this.vBox, this.packagesPerRow); }

    /**
     * Closes project details.
     */
    public void closeDetails(){
        this.projects.closeProjectDetails();
    }

    /**
     * Clears additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void clear(MainMenu mainMenu) {
        mainMenu.removeAdditionalPanel(this.panel);
    }

    /**
     * Sets additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void set(MainMenu mainMenu) {
        mainMenu.addAdditionalPanel(this.panel);
    }

    /**
     * Method that trigger package refresh after refreshing {@link core.LocalRepository}.
     */
    public void refreshPackages(){
        this.myMavenProject = FileOperator.loadProject(Paths.get(this.project.getProject().get("path").toString()));
        this.packagesAvailable.clear();
        this.initializePackages();
        this.triggerPageRefresh();
    }

    /**
     * Returns {@link Projects} instance.
     * @return {@link Projects} instance
     */
    public Projects getProjects(){ return this.projects; }

    /**
     * Return instance of project.
     * @return Instance of project
     */
    public Project getProject() { return project; }
}
