package graphics.sections;

import core.LocalPackage;
import graphics.menu.MainMenu;
import graphics.sections.components.ISection;
import graphics.sections.components.dashboard.Package;
import graphics.sections.components.packagedetails.Panel;
import graphics.sections.components.packagedetails.Version;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents package details.
 */
public class PackageDetails extends ScrollPane implements ISection {

    /**
     * Instance of main {@link Dashboard}.
     */
    private Dashboard dashboard;

    /**
     * Instance of package details panel.
     */
    private Panel panel;

    /**
     * Main layout pane.
     */
    private Pane pane;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * Package for detail information.
     */
    private Package pack;

    /**
     * List of available versions.
     */
    private List<Version> versionsAvailable;

    /**
     * Versions per row variable.
     */
    private double versionsPerRow = 1;

    /**
     * Initializes and creates package details.
     * @param pack Intance of package
     * @param dashboard Instance of dashboard
     */
    public PackageDetails(Package pack, Dashboard dashboard){
        this.pack = pack;
        this.dashboard = dashboard;
        this.initialize();
        this.setUp();
        this.initializeVersions();
    }

    /**
     * Initializes versions.
     */
    private void initializeVersions() {
        for(LocalPackage localPackage : this.pack.getVersionedLocalPackage().getVersions())
            this.addVersion(new Version(this, localPackage));
    }

    /**
     * Initializes package details.
     */
    private void initialize(){
        pane = new Pane();
        vBox = new VBox();
        this.versionsAvailable = new ArrayList<>();
        this.panel = new Panel(this);
    }

    /**
     * Setups package details.
     */
    private void setUp(){
        HBox hBox = new HBox();
        hBox.setSpacing(30);
        vBox.getChildren().add(hBox);
        vBox.setPadding(new Insets(30, 30, 30, 30));
        vBox.setSpacing(30);
        pane.getChildren().add(vBox);
        this.setContent(pane);
        this.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pane.setStyle("-fx-background-color: #fff");
    }

    /**
     * Adds new version.
     * @param version New version
     */
    public void addVersion(Version version) {
        this.versionsAvailable.add(version);
        Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
        if(hBox instanceof HBox){
            if(((HBox) hBox).getChildren().size() >= versionsPerRow){
                HBox newHbox = new HBox();
                newHbox.setSpacing(30);
                newHbox.getChildren().add(version);
                this.vBox.getChildren().add(newHbox);
            } else{
                ((HBox) hBox).getChildren().add(version);
            }
        } else{
            Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot add new package to the list - HBox did not found");
            alert.show();
        }
    }

    /**
     * Method that triggers version refresh.
     */
    public void refreshVersions(){
        List<LocalPackage> localPackages = this.pack.getVersionedLocalPackage().refresh();
        this.vBox.getChildren().clear();
        this.versionsAvailable.clear();
        HBox initialHBox = new HBox();
        initialHBox.setSpacing(30);
        this.vBox.getChildren().add(initialHBox);
        for(LocalPackage localPackage : localPackages)
            this.addVersion(new Version(this, localPackage));
        this.windowWidthChange(this.dashboard.getWindow().getStage().getWidth());
        this.dashboard.getWindow().getMainMenu().flashSuccessMessage("Versions refreshed");
    }

    /**
     * Width change event handler and recalculator.
     * @param newWidth New window width
     */
    public void windowWidthChange(double newWidth) {
        this.pane.setPrefWidth(newWidth - 30);
        Node nodeOut = pane.getChildren().get(0);
        if(nodeOut instanceof VBox){
            double versionsPerRow = Math.floor((this.dashboard.getWindow().getStage().getWidth() - 90) / (Version.MAX_WIDTH)) - 1;
            if(versionsPerRow == this.versionsPerRow) {
                for(Node nodeIn:((VBox)nodeOut).getChildren()){
                    if(nodeIn instanceof HBox){
                        ((HBox) nodeIn).setPrefWidth(this.pane.getWidth() - 60);
                        for (Node hBoxNodes : ((HBox) nodeIn).getChildren()) {
                            if (hBoxNodes instanceof GridPane) {
                                ((Version) hBoxNodes).widthChangeEventHandler();
                            }
                        }
                    }
                }
            } else reorganizeElements((VBox) nodeOut, versionsPerRow);
        }
    }

    /**
     * Recalculates and reorganizes elements.
     * @param nodeOut First {@link VBox} node
     * @param count Count of versions per row
     */
    private void reorganizeElements(VBox nodeOut, double count) {
        this.versionsPerRow = count;
        nodeOut.getChildren().clear();

        HBox initialHBox = new HBox();
        initialHBox.setSpacing(30);

        this.vBox.getChildren().add(initialHBox);

        for(Version version : this.versionsAvailable) {
            Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
            if (hBox instanceof HBox) {
                if (((HBox) hBox).getChildren().size() >= versionsPerRow) {
                    HBox newHbox = new HBox();
                    newHbox.setSpacing(30);
                    newHbox.getChildren().add(version);
                    this.vBox.getChildren().add(newHbox);
                } else {
                    ((HBox) hBox).getChildren().add(version);
                }
            }
        }

        this.windowWidthChange(this.dashboard.getWindow().getStage().getWidth());
    }

    /**
     * Height change event handler.
     * @param newHeight New window height
     */
    public void windowHeightChange(double newHeight) {
        this.pane.setMinHeight(newHeight - 180);
    }

    /**
     * Method triggers page refresh.
     */
    @Override
    public void triggerPageRefresh() { }

    /**
     * Closes package details.
     */
    public void closeDetails(){
        this.dashboard.closePackageDetails();
    }

    /**
     * Clears additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void clear(MainMenu mainMenu) {
        mainMenu.removeAdditionalPanel(this.panel);
    }

    /**
     * Sets additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void set(MainMenu mainMenu) {
        mainMenu.addAdditionalPanel(this.panel);
    }

    /**
     * Returns {@link Dashboard} instance.
     * @return {@link Dashboard} instance
     */
    public Dashboard getDashboard(){ return this.dashboard; }

    /**
     * Returns instance of package.
     * @return Instance of package
     */
    public Package getPackage(){ return this.pack; }
}
