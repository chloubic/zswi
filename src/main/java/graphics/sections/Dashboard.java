package graphics.sections;

import core.VersionedLocalPackage;
import graphics.Window;
import graphics.menu.MainMenu;
import graphics.sections.components.ISection;
import graphics.sections.components.dashboard.DashboardPagination;
import graphics.sections.components.dashboard.Filters;
import graphics.sections.components.dashboard.Package;
import graphics.sections.components.filters.SortOrder;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents dashboard of app.
 */
public class Dashboard extends ScrollPane implements ISection {

    /**
     * Instance of main {@link Window}.
     */
    private Window window;

    /**
     * Main layout pane.
     */
    private Pane pane;

    /**
     * Layout for elements.
     */
    private VBox vBox;

    /**
     * List of available packages.
     */
    private List<Package> packagesAvailable;

    /**
     * Instance of dashboard filters.
     */
    private Filters filters;

    /**
     * Instance of opened package details scene.
     */
    private PackageDetails packageDetails;

    /**
     * Instance of dashboard pagination.
     */
    private DashboardPagination dashboardPagination;

    /**
     * Packages per row variable.
     */
    private double packagesPerRow = 1;

    /**
     * Initializes and creates dashboard.
     * @param window Instance of {@link Window} scene
     */
    public Dashboard(Window window){
        super();
        this.window = window;
        this.initialize();
        this.setUp();
        if(window.getLocalRepository() != null)
            this.initializePackages();
    }

    /**
     * Initializes packages.
     */
    public void initializePackages() {
        for (VersionedLocalPackage pack : this.window.getLocalRepository().getVersionedPackages())
            this.addPackage(new Package(this, pack));
        this.dashboardPagination.setPageCount((int) Math.ceil(this.packagesAvailable.size() / (double) this.window.getConfigFile().getPostPerPage()));
        this.dashboardPagination.checkButtons();
        this.dashboardPagination.setPageInput(1);
    }

    /**
     * Initializes dashboard.
     */
    private void initialize() {
        pane = new Pane();
        vBox = new VBox();
        this.packagesAvailable = new ArrayList<>();
        this.filters = new Filters(this);
        this.dashboardPagination = new DashboardPagination(this);
    }

    /**
     * Setups dashboard.
     */
    private void setUp(){
        HBox hBox = new HBox();
        hBox.setSpacing(30);
        vBox.getChildren().add(hBox);
        vBox.setPadding(new Insets(30, 30, 30, 30));
        vBox.setSpacing(30);
        pane.getChildren().add(vBox);
        this.setContent(pane);
        this.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pane.setStyle("-fx-background-color: #fff");
    }

    /**
     * Adds new package.
     * @param pack New package
     */
    public void addPackage(Package pack) {
        this.packagesAvailable.add(pack);
        if(this.packagesAvailable.size() <= this.window.getConfigFile().getPostPerPage()) {
            Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
            if (hBox instanceof HBox) {
                if (((HBox) hBox).getChildren().size() >= packagesPerRow) {
                    HBox newHbox = new HBox();
                    newHbox.setSpacing(30);
                    newHbox.getChildren().add(pack);
                    this.vBox.getChildren().add(newHbox);
                } else {
                    ((HBox) hBox).getChildren().add(pack);
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot add new package to the list - HBox did not found");
                alert.show();
            }
        }
    }

    /**
     * Width change event handler and recalculator.
     * @param newWidth New window width
     */
    public void windowWidthChange(double newWidth) {
        this.pane.setPrefWidth(newWidth - 30);
        Node nodeOut = pane.getChildren().get(0);
        if(nodeOut instanceof VBox){
            double packagesPerRow = Math.floor((this.getWindow().getStage().getWidth() - 90) / (Package.MAX_WIDTH)) - 1;
            if(packagesPerRow == this.packagesPerRow) {
                for(Node nodeIn:((VBox)nodeOut).getChildren()){
                    if(nodeIn instanceof HBox){
                        ((HBox) nodeIn).setPrefWidth(this.pane.getWidth() - 60);
                        for (Node hBoxNodes : ((HBox) nodeIn).getChildren()) {
                            if (hBoxNodes instanceof GridPane) {
                                ((Package) hBoxNodes).widthChangeEventHandler();
                            }
                        }
                    }
                }
            } else reorganizeElements((VBox) nodeOut, packagesPerRow);
        }
    }

    /**
     * Recalculates and reorganizes elements.
     * @param nodeOut First {@link VBox} node
     * @param count Count of packages per row
     */
    private void reorganizeElements(VBox nodeOut, double count) {
        this.packagesPerRow = count;
        nodeOut.getChildren().clear();

        HBox initialHBox = new HBox();
        initialHBox.setSpacing(30);

        this.vBox.getChildren().add(initialHBox);

        int startIndex = (this.dashboardPagination.getActualPage() - 1) * this.window.getConfigFile().getPostPerPage();
        for(int i = startIndex; i < (startIndex + this.window.getConfigFile().getPostPerPage()); i++) {
            if(i < this.packagesAvailable.size()){
                Node hBox = this.vBox.getChildren().get(this.vBox.getChildren().size() - 1);
                if (hBox instanceof HBox) {
                    if (((HBox) hBox).getChildren().size() >= packagesPerRow) {
                        HBox newHbox = new HBox();
                        newHbox.setSpacing(30);
                        newHbox.getChildren().add(this.packagesAvailable.get(i));
                        this.vBox.getChildren().add(newHbox);
                    } else {
                        ((HBox) hBox).getChildren().add(this.packagesAvailable.get(i));
                    }
                }
            }
        }

        this.windowWidthChange(this.window.getStage().getWidth());
    }

    /**
     * Initilizes searched packages.
     * @param versionedLocalPackages List of searched packages
     */
    public void displaySearchedPackages(List<VersionedLocalPackage> versionedLocalPackages){
        this.dashboardPagination.setPageInput(1);
        this.packagesAvailable = new ArrayList<>();
        for(VersionedLocalPackage versionedLocalPackage : versionedLocalPackages)
            this.packagesAvailable.add(new Package(this, versionedLocalPackage));
        if(this.filters.getSortOrder().getSelectionModel().getSelectedItem().equals(SortOrder.DESCENDING))
            this.sortRevers();
        this.reorganizeElements(this.vBox, this.packagesPerRow);
        this.dashboardPagination.setPageCount((int)Math.ceil(this.packagesAvailable.size() / (double)this.window.getConfigFile().getPostPerPage()));
        this.dashboardPagination.checkButtons();
    }

    /**
     * Sorts available packages reverselly.
     */
    public void sortRevers(){
        ArrayList<Package> reversedList = new ArrayList<>();
        for(int i = this.packagesAvailable.size() - 1; i >= 0; i--)
            reversedList.add(this.packagesAvailable.get(i));
        this.packagesAvailable = reversedList;
    }

    /**
     * Height change event handler.
     * @param newHeight New window height
     */
    public void windowHeightChange(double newHeight) {
        this.pane.setMinHeight(newHeight - 175);
    }

    /**
     * Method triggers page refresh.
     */
    @Override
    public void triggerPageRefresh() {
        this.reorganizeElements(this.vBox, this.packagesPerRow);
    }

    /**
     * Clears additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void clear(MainMenu mainMenu) {
        mainMenu.removeAdditionalPanel(this.filters);
        this.window.setBottom(null);
    }

    /**
     * Sets additional panel.
     * @param mainMenu Main app menu
     */
    @Override
    public void set(MainMenu mainMenu) {
        mainMenu.addAdditionalPanel(this.filters);
        this.window.setBottom(this.dashboardPagination);
        if(this.packageDetails != null){
            this.window.clear();
            this.window.setActualSection(this.packageDetails);
            this.window.initializeContent();
        }
    }

    /**
     * Opens package details.
     * @param pack Package to be displayed
     */
    public void openPackageDetails(Package pack) {
        pack.getVersionedLocalPackage().sortVersions();
        this.packageDetails = new PackageDetails(pack, this);
        this.window.clear();
        this.window.setActualSection(this.packageDetails);
        this.window.initializeContent();
    }

    /**
     * Closes package details.
     */
    public void closePackageDetails() {
        this.packageDetails = null;
        this.window.clear();
        this.window.setActualSection(this);
        this.window.initializeContent();
        this.window.fireResizeEvent();
    }

    /**
     * Returns {@link Window} instance.
     * @return {@link Window} instance
     */
    public Window getWindow(){ return this.window; }

    /**
     * Returns list of available packages.
     * @return List of available packages
     */
    public List<Package> getAvailablePackages(){ return this.packagesAvailable; }

    /**
     * Returns actual package details instance.
     * @return Package details instance
     */
    public PackageDetails getPackageDetails(){ return this.packageDetails; }

    /**
     * Returns dashboard pagination instance.
     * @return Dashboard pagination instance
     */
    public DashboardPagination getDashboardPagination(){ return this.dashboardPagination; }

    /**
     * Sets actual package details instance.
     * @param packageDetails Package details instance
     */
    public void setPackageDetails(PackageDetails packageDetails){ this.packageDetails = packageDetails; }

}
