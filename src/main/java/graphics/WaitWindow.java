package graphics;

import core.LocalRepository;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Class represents wait window during async refreshing of Maven´s {@link LocalRepository}.
 */
public class WaitWindow extends Stage {

    /**
     * Main window container.
     */
    private VBox vBox;

    /**
     * Instance of loading message.
     */
    private Label label;

    /**
     * Instance of abort button.
     */
    private Button abortButton;

    /**
     * Running task.
     */
    private Task<LocalRepository> task;

    /**
     * Creates and initializes {@link WaitWindow}.
     */
    public WaitWindow(){
        super();
        this.initialize();
        this.setUp();
    }

    /**
     * Setups running task.
     * @param task Running task
     */
    public void setTask(Task<LocalRepository> task){ this.task = task; }

    /**
     * Setups window design.
     */
    private void setUp(){
        this.vBox.getChildren().addAll(label, abortButton);
        this.vBox.setStyle("-fx-background-color: #ffffff; -fx-padding: 10px 20px;-fx-border-color: #375e97; -fx-border-width: 3px");
        this.vBox.setAlignment(Pos.CENTER);
        this.vBox.setSpacing(20);

        this.label.setStyle("-fx-font-size: 12px; -fx-font-family: Verdana, Arial, sans-serif; -fx-font-weight: bold");
        this.label.setTextFill(Color.web("#375e97"));

        this.abortButton.setStyle("-fx-background-color: transparent; -fx-font-weight: bold; -fx-font-size: 12px");
        this.abortButton.setTextFill(Color.RED);
    }

    /**
     * Initializes window with components.
     */
    private void initialize(){
        this.vBox = new VBox();
        this.label = new Label("Loading local repository, please wait...");
        this.abortButton = new Button("Abort");

        this.abortButton.setOnMouseClicked(e -> {
            if(this.task != null) this.task.cancel();
        });

        Scene root = new Scene(vBox, 350, 100);
        this.setWidth(350);
        this.setHeight(100);
        this.setScene(root);
        this.setTitle("Loading | Maven Local Repository");
        this.setResizable(false);
        this.initStyle(StageStyle.UNDECORATED);
        this.initModality(javafx.stage.Modality.APPLICATION_MODAL);
    }

}
