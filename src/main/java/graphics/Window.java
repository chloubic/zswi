package graphics;

import configuration.ConfigFile;
import core.LocalRepository;
import graphics.menu.MainMenu;
import graphics.sections.Dashboard;
import graphics.sections.ProjectDetails;
import graphics.sections.Projects;
import graphics.sections.Settings;
import graphics.sections.components.ISection;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;

/**
 * Represents main control of window´s stage.
 * Procuring switching and plotting the correct scenes.
 */
public class Window extends BorderPane {

    /**
     * Instance of configuration file.
     */
    private ConfigFile configFile;

    /**
     * Actual instance of Maven´s {@link LocalRepository}.
     */
    private LocalRepository localRepository;

    /**
     * Instance of application stage.
     */
    private Stage stage;

    /**
     * Instance of application main menu on the top of app.
     */
    private MainMenu mainMenu;

    /**
     * Instance of {@link Dashboard}.
     */
    private Dashboard dashboard;

    /**
     * Instance of dashboard with projects.
     */
    private Projects projects;

    /**
     * Instance of application settings.
     */
    private Settings settings;

    /**
     * Instance of currently displayed section of the application.
     * All section implements {@link ISection} interface.
     */
    private ISection actualSection;

    /**
     * Instance of {@link WaitWindow} for async refreshing of {@link LocalRepository}.
     */
    private WaitWindow waitWindow;

    /**
     * Boolean variable for task success event.
     * Contains value for initialized/non-init app.
     */
    private boolean isInitialized;

    /**
     * Constructor setups all sections including main menu and initializes window with content.
     * @param stage Instance of the application {@link Stage}
     * @param waitWindow Instance of wait window
     */
    public Window(Stage stage, WaitWindow waitWindow){
        super();
        this.stage = stage;
        this.isInitialized = false;
        this.configFile = new ConfigFile(this);
        this.waitWindow = waitWindow;
        this.refreshRepository();
    }

    /**
     * Initializes main app scene on application startup.
     */
    private void initializeWindow() {
        this.setTop(this.mainMenu);
        this.setCenter(this.dashboard);
        this.setStyle("-fx-background-color: #ffffff");
    }

    /**
     * Setups currently displayed section into {@link #actualSection} variable.
     * @param section New {@link ISection} to display
     */
    public void setActualSection(ISection section) { this.actualSection = section; }

    /**
     * Initializes window content based on {@link #actualSection} value.
     */
    public void initializeContent(){
        this.setCenter((Node) this.actualSection);
        this.actualSection.set(this.mainMenu);
    }

    /**
     * Clears stage when scene is closing (including additional panels).
     */
    public void clear() {
        this.actualSection.clear(this.mainMenu);
        this.setCenter(null);
    }

    /**
     * Triggers recalculating of currently displayed scene.
     * @param newWidth New window width
     */
    public void windowWidthChange(double newWidth) { this.actualSection.windowWidthChange(newWidth); }

    /**
     * Triggers recalculating of currently displayed scene.
     * @param newHeight New window height
     */
    public void windowHeightChange(double newHeight) { this.actualSection.windowHeightChange(newHeight); }

    /**
     * Triggers or simulates resize events of the whole window.
     */
    public void fireResizeEvent(){
        this.windowWidthChange(this.stage.getWidth());
        this.windowHeightChange(this.stage.getHeight());
    }

    /**
     * Returns instance of dashboard.
     * @return Instance of dashboard
     */
    public Dashboard getDashboard() { return this.dashboard; }

    /**
     * Returns instance of application settings.
     * @return Instance of app settings
     */
    public Settings getSettings() { return this.settings; }

    /**
     * Returns instance of dashboard with projects.
     * @return Dashboard with projects
     */
    public Projects getProjects() { return this.projects; }

    /**
     * Return application stage.
     * @return Application stage
     */
    public Stage getStage(){ return this.stage; }

    /**
     * Returns instance of configuration file.
     * @return Instance of configuration file
     */
    public ConfigFile getConfigFile(){ return this.configFile; }

    /**
     * Returns instance of local repository.
     * @return Instance of local repository
     */
    public LocalRepository getLocalRepository(){ return this.localRepository; }

    /**
     * Returns instance of {@link ISection} interface, which represents actual section on top.
     * @return Instance of {@link ISection}/actual section
     */
    public ISection getActualSection(){ return this.actualSection; }

    /**
     * Returns instance of {@link MainMenu}.
     * @return Instance of {@link MainMenu}
     */
    public MainMenu getMainMenu() { return this.mainMenu; }

    /**
     * Asynchronous refreshing of Maven´s {@link LocalRepository}.
     */
    public void refreshRepository(){
        Task<LocalRepository> task = new Task<>() {
            @Override
            protected LocalRepository call() throws Exception {
                return LocalRepository.createInstance(Paths.get(configFile.getM2Path()));
            }
        };
        task.setOnSucceeded(e -> {
            try {
                localRepository = task.get();
                if(!isInitialized) {
                    this.dashboard = new Dashboard(this);
                    this.projects = new Projects(this);
                    this.settings = new Settings(this);
                    this.mainMenu = new MainMenu(this);
                    this.actualSection = dashboard;
                    this.initializeWindow();
                    this.initializeContent();
                    stage.show();
                    isInitialized = true;
                }
                else{
                    this.dashboard.getAvailablePackages().clear();
                    this.dashboard.initializePackages();
                    this.dashboard.triggerPageRefresh();
                    if(this.actualSection.equals(dashboard)
                            || this.actualSection.equals(this.dashboard.getPackageDetails()))
                        this.dashboard.closePackageDetails();
                    else
                        this.dashboard.setPackageDetails(null);

                    ProjectDetails projectDetails = this.projects.getProjectDetails();
                    if(projectDetails != null)
                        projectDetails.refreshPackages();

                    this.mainMenu.flashSuccessMessage("Repository refreshed");
                }
                waitWindow.hide();
                if(localRepository == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Instance of Local repository could not be created! Check validity of path to .m2 folder.");
                    alert.show();
                }
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            } catch (ExecutionException e1) {
                e1.printStackTrace();
            }
        });
        task.setOnCancelled(e -> {
            if(isInitialized){
                mainMenu.flashInfoMessage("Repository refresh canceled");
                waitWindow.hide();
            }
            else{
                Platform.exit();
                System.exit(0);
            }
        });
        waitWindow.setTask(task);
        waitWindow.show();
        (new Thread(task)).start();
    }
}