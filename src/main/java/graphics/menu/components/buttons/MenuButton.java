package graphics.menu.components.buttons;

import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents instance of application´s main menu button.
 */
public class MenuButton extends Button {

    /**
     * Initializes and creates menu instance of menu button.
     * @param buttonLabel Button label
     * @param icon Button icon
     */
    public MenuButton(String buttonLabel, Text icon){
        super(buttonLabel);
        this.setUp(icon);
        this.createDesign();
    }

    /**
     * Setups menu button icon.
     * @param icon Button icon
     */
    private void setUp(Text icon) { this.setGraphic(icon); }

    /**
     * Creates menu button design.
     */
    private void createDesign() {
        this.setStyle("-fx-font-size: 11px; -fx-background-color: #f8f8f8; -fx-padding: 7.5px 15px 7.5px 15px; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif");
        this.setTextFill(Color.web("#375e97"));
        ((Text) this.getGraphic()).setFill(Color.web("#375e97"));
    }

    /**
     * Mark button as currently used.
     */
    public void markAsCurrent(){
        this.setStyle("-fx-font-size: 11px; -fx-background-color: #375e97; -fx-padding: 7.5px 15px 7.5px 15px; -fx-font-weight: bold; -fx-font-family: Verdana, Arial, sans-serif");
        this.setTextFill(Color.WHITE);
        ((Text) this.getGraphic()).setFill(Color.web("#ffffff"));
    }

    /**
     * Mark button as currently not used.
     */
    public void unmarkAsCurrent(){ this.createDesign(); }

}
