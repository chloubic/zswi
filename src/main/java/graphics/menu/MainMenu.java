package graphics.menu;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import graphics.Window;
import graphics.menu.components.buttons.MenuButton;
import javafx.animation.AnimationTimer;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Represents main menu of the application on the top of window.
 */
public class MainMenu extends VBox {

    /**
     * Variable for timeout.
     */
    private Long lastUpdate;

    /**
     * Count of seconds for flashing message.
     */
    private final int CLOCK = 5;

    /**
     * Instance of main control window.
     */
    private Window window;

    /**
     * Instance of dashboard button.
     */
    private MenuButton dashboardButton;

    /**
     * Instance of projects button.
     */
    private MenuButton projectsButton;

    /**
     * Instance of settings button.
     */
    private MenuButton settingsButton;

    /**
     * Menu´s gridpane.
     */
    private GridPane gridPane;

    /**
     * Menu grid layout.
     */
    private HBox hBox;

    /**
     * Flash message label.
     */
    private Label infoLabel;

    /**
     * Flash timer.
     */
    private AnimationTimer timer;

    /**
     * Initializes and creates main menu layout.
     * Setups menu events.
     * @param window Instance of main control window
     */
    public MainMenu(Window window) {
        super();
        this.window = window;
        this.initializeButtons();
        this.initializeMenu();
        this.createButtonsDesign();
        this.setButtonsEventHandlers();
        this.initializeTimer();
    }

    /**
     * Initializes menu component´s structure.
     */
    private void initializeMenu() {
        this.gridPane = new GridPane();
        this.hBox = new HBox();
        this.infoLabel = new Label("");
        this.infoLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 14px; -fx-padding: 0 50px 0 0; -fx-font-family: Verdana, Arial, sans-serif");

        this.hBox.getChildren().addAll(this.dashboardButton, this.projectsButton, this.settingsButton);
        ColumnConstraints menuCons = new ColumnConstraints();
        ColumnConstraints infoCons = new ColumnConstraints();

        menuCons.setHgrow(Priority.NEVER);
        menuCons.setPrefWidth(500);
        infoCons.setHgrow(Priority.ALWAYS);
        infoCons.setHalignment(HPos.RIGHT);

        this.gridPane.getColumnConstraints().addAll(menuCons, infoCons);
        this.gridPane.add(hBox, 0, 0);
        this.gridPane.add(infoLabel, 1, 0);
        this.getChildren().add(this.gridPane);
    }

    /**
     * Initializes button´s instances with icons.
     */
    private void initializeButtons() {
        Text dashboardIcon = GlyphsDude.createIcon(FontAwesomeIcon.DASHBOARD, "16px");
        this.dashboardButton = new MenuButton("Dashboard", dashboardIcon);

        Text projectIcon = GlyphsDude.createIcon(FontAwesomeIcon.PRODUCT_HUNT, "16px");
        this.projectsButton = new MenuButton("Projects", projectIcon);

        Text settingsIcon = GlyphsDude.createIcon(FontAwesomeIcon.GEAR, "16px");
        this.settingsButton = new MenuButton("Settings", settingsIcon);
    }

    /**
     * Creates button´s layout design.
     */
    private void createButtonsDesign() {
        this.hBox.setSpacing(30);
        this.hBox.setPadding(new Insets(10));

        this.dashboardButton.markAsCurrent();
    }

    /**
     * Setups buttons click events.
     */
    private void setButtonsEventHandlers(){
        this.settingsButton.setOnMouseClicked(e -> {
            this.clearSelection();
            this.settingsButton.markAsCurrent();
            this.window.clear();
            this.window.setActualSection(this.window.getSettings());
            this.window.initializeContent();
            this.window.fireResizeEvent();
        });
        this.projectsButton.setOnMouseClicked(e -> {
            if(this.window.getActualSection().equals(this.window.getProjects().getProjectDetails()))
                this.window.getProjects().closeProjectDetails();
            this.clearSelection();
            this.projectsButton.markAsCurrent();
            this.window.clear();
            this.window.setActualSection(this.window.getProjects());
            this.window.initializeContent();
            this.window.fireResizeEvent();
        });
        this.dashboardButton.setOnMouseClicked(e -> {
            if(this.window.getActualSection().equals(this.window.getDashboard().getPackageDetails()))
                this.window.getDashboard().closePackageDetails();
            this.clearSelection();
            this.dashboardButton.markAsCurrent();
            this.window.clear();
            this.window.setActualSection(this.window.getDashboard());
            this.window.initializeContent();
            this.window.fireResizeEvent();
        });
    }

    /**
     * Marks all buttons as not displayed.
     */
    private void clearSelection() {
        this.dashboardButton.unmarkAsCurrent();
        this.projectsButton.unmarkAsCurrent();
        this.settingsButton.unmarkAsCurrent();
    }

    /**
     * Adds additional panel of currently displayed section.
     * @param additionalPanel Additional panel of currently displayed section
     */
    public void addAdditionalPanel(IAdditionalPanel additionalPanel){ this.getChildren().add((Node) additionalPanel); }

    /**
     * Removes additional panel of currently displayed section.
     * @param additionalPanel Additional panel of currently displayed section
     */
    public void removeAdditionalPanel(IAdditionalPanel additionalPanel){ this.getChildren().remove(additionalPanel); }

    /**
     * Iinitializes flash timer.
     */
    private void initializeTimer(){
        this.timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                double elapsedTime = (now - lastUpdate.longValue()) / 1000000000.0;
                if(elapsedTime >= CLOCK) {
                    infoLabel.setTextFill(Color.WHITE);
                    timer.stop();
                }
            }
        };
    }

    /**
     * Flashes success message.
     * @param message Message text
     */
    public void flashSuccessMessage(String message){
        this.infoLabel.setText(message);
        this.infoLabel.setTextFill(Color.web("#00a90c"));
        this.lastUpdate = System.nanoTime();
        this.timer.start();
    }

    /**
     * Flashes fail message.
     * @param message Message text
     */
    public void flashFailMessage(String message){
        this.infoLabel.setText(message);
        this.infoLabel.setTextFill(Color.web("#ff0000"));
        this.lastUpdate = System.nanoTime();
        this.timer.start();
    }

    /**
     * Flashes info message.
     * @param message Message text
     */
    public void flashInfoMessage(String message){
        this.infoLabel.setText(message);
        this.infoLabel.setTextFill(Color.web("#375e97"));
        this.lastUpdate = System.nanoTime();
        this.timer.start();
    }

}